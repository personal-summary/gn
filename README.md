# GN基础语法介绍

## 目录

- [概述](#概述)
- [GN命令列表](#GN命令列表)

## 一、概述<a name="概述"></a>

​		本文档主要介绍OpenHarmony编译常用的gn工具语法介绍

## 二、GN命令列表<a name="GN命令列表"></a>
### 2.1 gn help analyze

- [英文](./en/analyze.md)
- [中文](./cn/analyze.md)
- 状态：todo

### 2.2 gn help args

- [英文](./en/args.md)
- [中文](./cn/args.md)
- 状态：todo

### 2.3 gn help check

- [英文](./en/check.md)
- [中文](./cn/check.md)
- 状态：todo

### 2.4 gn help clean

- [英文](./en/clean.md)
- [中文](./cn/clean.md)
- 状态：done

### 2.5 gn help desc

- [英文](./en/desc.md)
- [中文](./cn/desc.md)
- 状态：todo

### 2.6 gn help format

- [英文](./en/format.md)
- [中文](./cn/format.md)
- 状态：done

### 2.7 gn help gen

- [英文](./en/gen.md)
- [中文](./cn/gen.md)
- 状态：done

### 2.8 gn help help

- [英文](./en/help.md)
- [中文](./cn/help.md)
- 状态：done

### 2.9 gn help ls

- [英文](./en/ls.md)
- [中文](./cn/ls.md)
- 状态：todo

### 2.10 gn help meta

- [英文](./en/meta.md)
- [中文](./cn/meta.md)
- 状态：todo

### 2.11 gn help path

- [英文](./en/path.md)
- [中文](./cn/path.md)
- 状态：done

### 2.12 gn help refs

- [英文](./en/refs.md)
- [中文](./cn/refs.md)
- 状态：todo

### 2.13 gn help action

- [英文](./en/action.md)
- [中文](./cn/action.md)
- 状态：todo

### 2.14 gn help action_foreach

- [英文](./en/action_foreach.md)
- [中文](./cn/action_foreach.md)
- 状态：todo

### 2.15 gn help bundle_data

- [英文](./en/bundle_data.md)
- [中文](./cn/bundle_data.md)
- 状态：todo

### 2.16 gn help copy

- [英文](./en/copy.md)
- [中文](./cn/copy.md)
- 状态：done

### 2.17 gn help create_bundle

- [英文](./en/create_bundle.md)
- [中文](./cn/create_bundle.md)
- 状态：todo

### 2.18 gn help executable

- [英文](./en/executable.md)
- [中文](./cn/executable.md)
- 状态：todo

### 2.19 gn help generated_file

- [英文](./en/generated_file.md)
- [中文](./cn/generated_file.md)
- 状态：todo

### 2.20 gn help group

- [英文](./en/group.md)
- [中文](./cn/group.md)
- 状态：todo

### 2.21 gn help loadable_module

- [英文](./en/loadable_module.md)
- [中文](./cn/loadable_module.md)
- 状态：todo

### 2.22 gn help rust_library

- [英文](./en/rust_library.md)
- [中文](./cn/rust_library.md)
- 状态：todo

### 2.23 gn help rust_proc_macro

- [英文](./en/rust_proc_macro.md)
- [中文](./cn/rust_proc_macro.md)
- 状态：todo

### 2.24 gn help shared_library

- [英文](./en/shared_library.md)
- [中文](./cn/shared_library.md)
- 状态：todo

### 2.25 gn help source_set

- [英文](./en/source_set.md)
- [中文](./cn/source_set.md)
- 状态：todo

### 2.26 gn help static_library

- [英文](./en/static_library.md)
- [中文](./cn/static_library.md)
- 状态：todo

### 2.27 gn help target

- [英文](./en/target.md)
- [中文](./cn/target.md)
- 状态：todo

### 2.28 gn help assert

- [英文](./en/assert.md)
- [中文](./cn/assert.md)
- 状态：todo

### 2.29 gn help config

- [英文](./en/config.md)
- [中文](./cn/config.md)
- 状态：todo

### 2.30 gn help declare_args

- [英文](./en/declare_args.md)
- [中文](./cn/declare_args.md)
- 状态：todo

### 2.31 gn help defined

- [英文](./en/defined.md)
- [中文](./cn/defined.md)
- 状态：todo

### 2.32 gn help exec_script

- [英文](./en/exec_script.md)
- [中文](./cn/exec_script.md)
- 状态：todo

### 2.33 gn help foreach

- [英文](./en/foreach.md)
- [中文](./cn/foreach.md)
- 状态：todo

### 2.34 gn help forward_variables_from

- [英文](./en/forward_variables_from.md)
- [中文](./cn/forward_variables_from.md)
- 状态：todo

### 2.35 gn help get_label_info

- [英文](./en/get_label_info.md)
- [中文](./cn/get_label_info.md)
- 状态：todo

### 2.36 gn help get_path_info

- [英文](./en/get_path_info.md)
- [中文](./cn/get_path_info.md)
- 状态：todo

### 2.37 gn help get_target_outputs

- [英文](./en/get_target_outputs.md)
- [中文](./cn/get_target_outputs.md)
- 状态：todo

### 2.38 gn help getenv

- [英文](./en/getenv.md)
- [中文](./cn/getenv.md)
- 状态：todo

### 2.39 gn help import

- [英文](./en/import.md)
- [中文](./cn/import.md)
- 状态：todo

### 2.40 gn help not_needed

- [英文](./en/not_needed.md)
- [中文](./cn/not_needed.md)
- 状态：todo

### 2.41 gn help pool

- [英文](./en/pool.md)
- [中文](./cn/pool.md)
- 状态：todo

### 2.42 gn help print

- [英文](./en/print.md)
- [中文](./cn/print.md)
- 状态：todo

### 2.43 gn help process_file_template

- [英文](./en/process_file_template.md)
- [中文](./cn/process_file_template.md)
- 状态：todo

### 2.44 gn help read_file

- [英文](./en/read_file.md)
- [中文](./cn/read_file.md)
- 状态：todo

### 2.45 gn help rebase_path

- [英文](./en/rebase_path.md)
- [中文](./cn/rebase_path.md)
- 状态：todo

### 2.46 gn help set_default_toolchain

- [英文](./en/set_default_toolchain.md)
- [中文](./cn/set_default_toolchain.md)
- 状态：todo

### 2.47 gn help set_defaults

- [英文](./en/set_defaults.md)
- [中文](./cn/set_defaults.md)
- 状态：todo

### 2.48 gn help set_sources_assignment_filter

- [英文](./en/set_sources_assignment_filter.md)
- [中文](./cn/set_sources_assignment_filter.md)
- 状态：todo

### 2.49 gn help split_list

- [英文](./en/split_list.md)
- [中文](./cn/split_list.md)
- 状态：todo

### 2.50 gn help string_join

- [英文](./en/string_join.md)
- [中文](./cn/string_join.md)
- 状态：todo

### 2.51 gn help string_replace

- [英文](./en/string_replace.md)
- [中文](./cn/string_replace.md)
- 状态：todo

### 2.52 gn help string_split

- [英文](./en/string_split.md)
- [中文](./cn/string_split.md)
- 状态：todo

### 2.53 gn help template

- [英文](./en/template.md)
- [中文](./cn/template.md)
- 状态：todo

### 2.54 gn help tool

- [英文](./en/tool.md)
- [中文](./cn/tool.md)
- 状态：todo

### 2.55 gn help toolchain

- [英文](./en/toolchain.md)
- [中文](./cn/toolchain.md)
- 状态：todo

### 2.56 gn help write_file

- [英文](./en/write_file.md)
- [中文](./cn/write_file.md)
- 状态：todo

### 2.57 gn help current_cpu

- [英文](./en/current_cpu.md)
- [中文](./cn/current_cpu.md)
- 状态：todo

### 2.58 gn help current_os

- [英文](./en/current_os.md)
- [中文](./cn/current_os.md)
- 状态：todo

### 2.59 gn help current_toolchain

- [英文](./en/current_toolchain.md)
- [中文](./cn/current_toolchain.md)
- 状态：todo

### 2.60 gn help default_toolchain

- [英文](./en/default_toolchain.md)
- [中文](./cn/default_toolchain.md)
- 状态：todo

### 2.61 gn help gn_version

- [英文](./en/gn_version.md)
- [中文](./cn/gn_version.md)
- 状态：todo

### 2.62 gn help host_cpu

- [英文](./en/host_cpu.md)
- [中文](./cn/host_cpu.md)
- 状态：todo

### 2.63 gn help host_os

- [英文](./en/host_os.md)
- [中文](./cn/host_os.md)
- 状态：todo

### 2.64 gn help invoker

- [英文](./en/invoker.md)
- [中文](./cn/invoker.md)
- 状态：todo

### 2.65 gn help python_path

- [英文](./en/python_path.md)
- [中文](./cn/python_path.md)
- 状态：todo

### 2.66 gn help root_build_dir

- [英文](./en/root_build_dir.md)
- [中文](./cn/root_build_dir.md)
- 状态：todo

### 2.67 gn help root_gen_dir

- [英文](./en/root_gen_dir.md)
- [中文](./cn/root_gen_dir.md)
- 状态：todo

### 2.68 gn help root_out_dir

- [英文](./en/root_out_dir.md)
- [中文](./cn/root_out_dir.md)
- 状态：todo

### 2.69 gn help target_cpu

- [英文](./en/target_cpu.md)
- [中文](./cn/target_cpu.md)
- 状态：todo

### 2.70 gn help target_gen_dir

- [英文](./en/target_gen_dir.md)
- [中文](./cn/target_gen_dir.md)
- 状态：todo

### 2.71 gn help target_name

- [英文](./en/target_name.md)
- [中文](./cn/target_name.md)
- 状态：todo

### 2.72 gn help target_os

- [英文](./en/target_os.md)
- [中文](./cn/target_os.md)
- 状态：todo

### 2.73 gn help target_out_dir

- [英文](./en/target_out_dir.md)
- [中文](./cn/target_out_dir.md)
- 状态：todo

### 2.74 gn help aliased_deps

- [英文](./en/aliased_deps.md)
- [中文](./cn/aliased_deps.md)
- 状态：todo

### 2.75 gn help all_dependent_configs

- [英文](./en/all_dependent_configs.md)
- [中文](./cn/all_dependent_configs.md)
- 状态：todo

### 2.76 gn help allow_circular_includes_from

- [英文](./en/allow_circular_includes_from.md)
- [中文](./cn/allow_circular_includes_from.md)
- 状态：todo

### 2.77 gn help arflags

- [英文](./en/arflags.md)
- [中文](./cn/arflags.md)
- 状态：todo

### 2.78 gn help args

- [英文](./en/args.md)
- [中文](./cn/args.md)
- 状态：todo

### 2.79 gn help asmflags

- [英文](./en/asmflags.md)
- [中文](./cn/asmflags.md)
- 状态：todo

### 2.80 gn help assert_no_deps

- [英文](./en/assert_no_deps.md)
- [中文](./cn/assert_no_deps.md)
- 状态：todo

### 2.81 gn help bundle_contents_dir

- [英文](./en/bundle_contents_dir.md)
- [中文](./cn/bundle_contents_dir.md)
- 状态：todo

### 2.82 gn help bundle_deps_filter

- [英文](./en/bundle_deps_filter.md)
- [中文](./cn/bundle_deps_filter.md)
- 状态：todo

### 2.83 gn help bundle_executable_dir

- [英文](./en/bundle_executable_dir.md)
- [中文](./cn/bundle_executable_dir.md)
- 状态：todo

### 2.84 gn help bundle_resources_dir

- [英文](./en/bundle_resources_dir.md)
- [中文](./cn/bundle_resources_dir.md)
- 状态：todo

### 2.85 gn help bundle_root_dir

- [英文](./en/bundle_root_dir.md)
- [中文](./cn/bundle_root_dir.md)
- 状态：todo

### 2.86 gn help cflags

- [英文](./en/cflags.md)
- [中文](./cn/cflags.md)
- 状态：todo

### 2.87 gn help cflags_c

- [英文](./en/cflags_c.md)
- [中文](./cn/cflags_c.md)
- 状态：todo

### 2.88 gn help cflags_cc

- [英文](./en/cflags_cc.md)
- [中文](./cn/cflags_cc.md)
- 状态：todo

### 2.89 gn help cflags_objc

- [英文](./en/cflags_objc.md)
- [中文](./cn/cflags_objc.md)
- 状态：todo

### 2.90 gn help cflags_objcc

- [英文](./en/cflags_objcc.md)
- [中文](./cn/cflags_objcc.md)
- 状态：todo

### 2.91 gn help check_includes

- [英文](./en/check_includes.md)
- [中文](./cn/check_includes.md)
- 状态：todo

### 2.92 gn help code_signing_args

- [英文](./en/code_signing_args.md)
- [中文](./cn/code_signing_args.md)
- 状态：todo

### 2.93 gn help code_signing_outputs

- [英文](./en/code_signing_outputs.md)
- [中文](./cn/code_signing_outputs.md)
- 状态：todo

### 2.94 gn help code_signing_script

- [英文](./en/code_signing_script.md)
- [中文](./cn/code_signing_script.md)
- 状态：todo

### 2.95 gn help code_signing_sources

- [英文](./en/code_signing_sources.md)
- [中文](./cn/code_signing_sources.md)
- 状态：todo

### 2.96 gn help complete_static_lib

- [英文](./en/complete_static_lib.md)
- [中文](./cn/complete_static_lib.md)
- 状态：todo

### 2.97 gn help configs

- [英文](./en/configs.md)
- [中文](./cn/configs.md)
- 状态：todo

### 2.98 gn help contents

- [英文](./en/contents.md)
- [中文](./cn/contents.md)
- 状态：todo

### 2.99 gn help crate_name

- [英文](./en/crate_name.md)
- [中文](./cn/crate_name.md)
- 状态：todo

### 2.100 gn help crate_root

- [英文](./en/crate_root.md)
- [中文](./cn/crate_root.md)
- 状态：todo

### 2.101 gn help crate_type

- [英文](./en/crate_type.md)
- [中文](./cn/crate_type.md)
- 状态：todo

### 2.102 gn help data

- [英文](./en/data.md)
- [中文](./cn/data.md)
- 状态：todo

### 2.103 gn help data_deps

- [英文](./en/data_deps.md)
- [中文](./cn/data_deps.md)
- 状态：todo

### 2.104 gn help data_keys

- [英文](./en/data_keys.md)
- [中文](./cn/data_keys.md)
- 状态：todo

### 2.105 gn help defines

- [英文](./en/defines.md)
- [中文](./cn/defines.md)
- 状态：todo

### 2.106 gn help depfile

- [英文](./en/depfile.md)
- [中文](./cn/depfile.md)
- 状态：todo

### 2.107 gn help deps

- [英文](./en/deps.md)
- [中文](./cn/deps.md)
- 状态：todo

### 2.108 gn help externs

- [英文](./en/externs.md)
- [中文](./cn/externs.md)
- 状态：todo

### 2.109 gn help framework_dirs

- [英文](./en/framework_dirs.md)
- [中文](./cn/framework_dirs.md)
- 状态：todo

### 2.110 gn help frameworks

- [英文](./en/frameworks.md)
- [中文](./cn/frameworks.md)
- 状态：todo

### 2.111 gn help friend

- [英文](./en/friend.md)
- [中文](./cn/friend.md)
- 状态：todo

### 2.112 gn help include_dirs

- [英文](./en/include_dirs.md)
- [中文](./cn/include_dirs.md)
- 状态：todo

### 2.113 gn help inputs

- [英文](./en/inputs.md)
- [中文](./cn/inputs.md)
- 状态：todo

### 2.114 gn help ldflags

- [英文](./en/ldflags.md)
- [中文](./cn/ldflags.md)
- 状态：todo

### 2.115 gn help lib_dirs

- [英文](./en/lib_dirs.md)
- [中文](./cn/lib_dirs.md)
- 状态：todo

### 2.116 gn help libs

- [英文](./en/libs.md)
- [中文](./cn/libs.md)
- 状态：todo

### 2.117 gn help metadata

- [英文](./en/metadata.md)
- [中文](./cn/metadata.md)
- 状态：todo

### 2.118 gn help output_conversion

- [英文](./en/output_conversion.md)
- [中文](./cn/output_conversion.md)
- 状态：todo

### 2.119 gn help output_dir

- [英文](./en/output_dir.md)
- [中文](./cn/output_dir.md)
- 状态：todo

### 2.120 gn help output_extension

- [英文](./en/output_extension.md)
- [中文](./cn/output_extension.md)
- 状态：todo

### 2.121 gn help output_name

- [英文](./en/output_name.md)
- [中文](./cn/output_name.md)
- 状态：todo

### 2.122 gn help output_prefix_override

- [英文](./en/output_prefix_override.md)
- [中文](./cn/output_prefix_override.md)
- 状态：todo

### 2.123 gn help outputs

- [英文](./en/outputs.md)
- [中文](./cn/outputs.md)
- 状态：todo

### 2.124 gn help partial_info_plist

- [英文](./en/partial_info_plist.md)
- [中文](./cn/partial_info_plist.md)
- 状态：todo

### 2.125 gn help pool

- [英文](./en/pool.md)
- [中文](./cn/pool.md)
- 状态：todo

### 2.126 gn help precompiled_header

- [英文](./en/precompiled_header.md)
- [中文](./cn/precompiled_header.md)
- 状态：todo

### 2.127 gn help precompiled_header_type

- [英文](./en/precompiled_header_type.md)
- [中文](./cn/precompiled_header_type.md)
- 状态：todo

### 2.128 gn help precompiled_source

- [英文](./en/precompiled_source.md)
- [中文](./cn/precompiled_source.md)
- 状态：todo

### 2.129 gn help product_type

- [英文](./en/product_type.md)
- [中文](./cn/product_type.md)
- 状态：todo

### 2.130 gn help public

- [英文](./en/public.md)
- [中文](./cn/public.md)
- 状态：todo

### 2.131 gn help public_configs

- [英文](./en/public_configs.md)
- [中文](./cn/public_configs.md)
- 状态：todo

### 2.132 gn help public_deps

- [英文](./en/public_deps.md)
- [中文](./cn/public_deps.md)
- 状态：todo

### 2.133 gn help rebase

- [英文](./en/rebase.md)
- [中文](./cn/rebase.md)
- 状态：todo

### 2.134 gn help response_file_contents

- [英文](./en/response_file_contents.md)
- [中文](./cn/response_file_contents.md)
- 状态：todo

### 2.135 gn help script

- [英文](./en/script.md)
- [中文](./cn/script.md)
- 状态：todo

### 2.136 gn help sources

- [英文](./en/sources.md)
- [中文](./cn/sources.md)
- 状态：todo

### 2.137 gn help testonly

- [英文](./en/testonly.md)
- [中文](./cn/testonly.md)
- 状态：todo

### 2.138 gn help visibility

- [英文](./en/visibility.md)
- [中文](./cn/visibility.md)
- 状态：todo

### 2.139 gn help walk_keys

- [英文](./en/walk_keys.md)
- [中文](./cn/walk_keys.md)
- 状态：todo

### 2.140 gn help write_runtime_deps

- [英文](./en/write_runtime_deps.md)
- [中文](./cn/write_runtime_deps.md)
- 状态：todo

### 2.141 gn help xcode_extra_attributes

- [英文](./en/xcode_extra_attributes.md)
- [中文](./cn/xcode_extra_attributes.md)
- 状态：todo

### 2.142 gn help xcode_test_application_name

- [英文](./en/xcode_test_application_name.md)
- [中文](./cn/xcode_test_application_name.md)
- 状态：todo

### 2.143 gn help all

- [英文](./en/all.md)
- [中文](./cn/all.md)
- 状态：todo

### 2.144 gn help buildargs

- [英文](./en/buildargs.md)
- [中文](./cn/buildargs.md)
- 状态：todo

### 2.145 gn help dotfile

- [英文](./en/dotfile.md)
- [中文](./cn/dotfile.md)
- 状态：todo

### 2.146 gn help execution

- [英文](./en/execution.md)
- [中文](./cn/execution.md)
- 状态：todo

### 2.147 gn help grammar

- [英文](./en/grammar.md)
- [中文](./cn/grammar.md)
- 状态：todo

### 2.148 gn help label_pattern

- [英文](./en/label_pattern.md)
- [中文](./cn/label_pattern.md)
- 状态：todo

### 2.149 gn help labels

- [英文](./en/labels.md)
- [中文](./cn/labels.md)
- 状态：todo

### 2.150 gn help metadata_collection

- [英文](./en/metadata_collection.md)
- [中文](./cn/metadata_collection.md)
- 状态：todo

### 2.151 gn help ninja_rules

- [英文](./en/ninja_rules.md)
- [中文](./cn/ninja_rules.md)
- 状态：todo

### 2.152 gn help nogncheck

- [英文](./en/nogncheck.md)
- [中文](./cn/nogncheck.md)
- 状态：todo

### 2.153 gn help output_conversion

- [英文](./en/output_conversion.md)
- [中文](./cn/output_conversion.md)
- 状态：todo

### 2.154 gn help runtime_deps

- [英文](./en/runtime_deps.md)
- [中文](./cn/runtime_deps.md)
- 状态：todo

### 2.155 gn help source_expansion

- [英文](./en/source_expansion.md)
- [中文](./cn/source_expansion.md)
- 状态：todo
