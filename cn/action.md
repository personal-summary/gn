### gn help action
#### **action**: Declare a target that runs a script a single time.

运行：声明一次运行脚本的目标。

```
  This target type allows you to run a script a single time to produce one or
  more output files. If you want to run a script once for each of a set of
  input files, see "gn help action_foreach".
  
  此目标类型允许您一次运行脚本以生成一个或更多输出文件。
  如果要为一组输入文件，请参阅“gn help action_foreach”。
  
```

#### **Inputs**

```
  In an action the "sources" and "inputs" are treated the same: they're both
  input dependencies on script execution with no special handling. If you want
  to pass the sources to your script, you must do so explicitly by including
  them in the "args". Note also that this means there is no special handling of
  paths since GN doesn't know which of the args are paths and not. You will
  want to use rebase_path() to convert paths to be relative to the
  root_build_dir.

  You can dynamically write input dependencies (for incremental rebuilds if an
  input file changes) by writing a depfile when the script is run (see "gn help
  depfile"). This is more flexible than "inputs".

  If the command line length is very long, you can use response files to pass
  args to your script. See "gn help response_file_contents".

  It is recommended you put inputs to your script in the "sources" variable,
  and stuff like other Python files required to run your script in the "inputs"
  variable.

  The "deps" and "public_deps" for an action will always be
  completed before any part of the action is run so it can depend on
  the output of previous steps. The "data_deps" will be built if the
  action is built, but may not have completed before all steps of the
  action are started. This can give additional parallelism in the build
  for runtime-only dependencies.
  
   在一次运行中，“源”和“输入”被视为相同的：它们都是输入依赖于脚本执行，无需特殊处理。如果你愿意要将源传递给脚本，必须通过包含他们在“args”中。还请注意，这意    味着没有特殊处理路径，因为GN不知道哪些参数是路径。你会希望使用rebase_path（）将路径转换为相对于root_build_dir。
   您可以动态写入输入依赖项（对于增量重建，如果输入文件更改），方法是在脚本运行时编写depfile（请参阅“gn帮助”depfile”）。这比“inputs”更灵活。
   如果命令行长度很长，可以使用响应文件传递脚本的参数。请参阅“gn help response_contents”。
   建议您在“sources”变量中输入脚本，以及在“inputs”中运行脚本所需的其他Python文件变量
   操作的“deps”和“public_deps”将始终为在操作的任何部分运行之前完成，因此它可以依赖于先前步骤的输出。如果操作已生成，但可能尚未在动作开始。这可以在构建中提供额外的并行性仅用于运行时依赖项。
```

#### **Outputs**

```
  You should specify files created by your script by specifying them in the
  "outputs".

  The script will be executed with the given arguments with the current
  directory being that of the root build directory. If you pass files
  to your script, see "gn help rebase_path" for how to convert
  file names to be relative to the build directory (file names in the
  sources, outputs, and inputs will be all treated as relative to the
  current build file and converted as needed automatically).

  GN sets Ninja's flag 'restat = 1` for all action commands. This means
  that Ninja will check the timestamp of the output after the action
  completes. If output timestamp is unchanged, the step will be treated
  as if it never needed to be rebuilt, potentially eliminating some
  downstream steps for incremental builds. Scripts can improve build
  performance by taking care not to change the timstamp of the output
  file(s) if the contents have not changed.
  
  您应该通过在“输出”中指定脚本创建的文件来指定它们。
  脚本将使用给定的参数执行，当前目录是根构建目录的当前目录。
  如果将文件传递给脚本，请参阅“gn help rebase_path”了解如何转换与构建目录相关的文件名（源、输出和输入中的文件名都将被视为与当前构建文件相关，并根据需要自动转换）。
  GN为所有动作命令设置忍者标志“restat=1”。这意味着Ninja将在操作完成后检查输出的时间戳。如果输出时间戳不变，则该步骤将被视为从未需要重新构建，从而可能消除增量构建的一些下游步骤。脚本可以通过注意在内容未更改的情况下不更改输出文件的时间戳来提高生成性能。
  
  
```

#### **File name handling**

```
  All output files must be inside the output directory of the build.
  You would generally use |$target_out_dir| or |$target_gen_dir| to
  reference the output or generated intermediate file directories,
  respectively.
  所有输出文件必须位于生成的输出目录中。您通常会使用|$target_out_dir|或|$target_gen_dir|分别引用输出或生成的中间文件目录。
  
```

#### **Variables**

```
  args, data, data_deps, depfile, deps, inputs, metadata, outputs*, pool,
  response_file_contents, script*, sources
  * = required
```

#### **Example**

```
  action("run_this_guy_once") {
    script = "doprocessing.py"
    sources = [ "my_configuration.txt" ]
    outputs = [ "$target_gen_dir/insightful_output.txt" ]

    # Our script imports this Python file so we want to rebuild if it changes.
    inputs = [ "helper_library.py" ]

    # Note that we have to manually pass the sources to our script if the
    # script needs them as inputs.
    args = [ "--out", rebase_path(target_gen_dir, root_build_dir) ] +
           rebase_path(sources, root_build_dir)
  }
```
