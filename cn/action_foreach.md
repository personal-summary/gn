### gn help action_foreach
#### **action_foreach**: Declare a target that runs a script over a set of files.

声明在一组文件上运行脚本的目标

```
  This target type allows you to run a script once-per-file over a set of
  sources. If you want to run a script once that takes many files as input, see
  "gn help action".
  此目标类型允许您在一组源上对每个文件运行一次脚本。如果您想运行一次以多个文件为输入的脚本，请参阅“gn help action”。
```

#### **Inputs**

```
  The script will be run once per file in the "sources" variable. The "outputs"
  variable should specify one or more files with a source expansion pattern in
  it (see "gn help source_expansion"). The output file(s) for each script
  invocation should be unique. Normally you use "{{source_name_part}}" in each
  output file.

  If your script takes additional data as input, such as a shared configuration
  file or a Python module it uses, those files should be listed in the "inputs"
  variable. These files are treated as dependencies of each script invocation.

  If the command line length is very long, you can use response files to pass
  args to your script. See "gn help response_file_contents".

  You can dynamically write input dependencies (for incremental rebuilds if an
  input file changes) by writing a depfile when the script is run (see "gn help
  depfile"). This is more flexible than "inputs".

  The "deps" and "public_deps" for an action will always be
  completed before any part of the action is run so it can depend on
  the output of previous steps. The "data_deps" will be built if the
  action is built, but may not have completed before all steps of the
  action are started. This can give additional parallelism in the build
  for runtime-only dependencies.
  
  脚本将在“sources”变量中对每个文件运行一次。“outputs”变量应指定一个或多个具有源扩展模式的文件（请参阅“gn help source_expansion”）。每个脚本调用的输出文件应该是唯一的。通常在每个输出文件中使用“｛｛source_name_part｝｝”。
  如果脚本将其他数据作为输入，例如共享配置文件或它使用的Python模块，则这些文件应列在“inputs”变量中。这些文件被视为每个脚本调用的依赖项。
  如果命令行长度很长，可以使用响应文件将参数传递给脚本。请参阅“gn help response_contents”。 
  通过在脚本运行时写入depfile（请参阅“gn help depfile”），可以动态地写入输入依赖项（如果输入文件发生更改，则用于增量重建）。这比“输入”更灵活。
  操作的“deps”和“public_deps”将始终在操作的任何部分运行之前完成，因此它可以依赖于前面步骤的输出。如果构建了操作，则将构建“data_deps”，但在操作的所有步骤开始之前可能尚未完成。这可以为仅运行时依赖项的构建提供额外的并行性。
  
  
```

#### **Outputs**

```
  The script will be executed with the given arguments with the current
  directory being that of the root build directory. If you pass files
  to your script, see "gn help rebase_path" for how to convert
  file names to be relative to the build directory (file names in the
  sources, outputs, and inputs will be all treated as relative to the
  current build file and converted as needed automatically).

  GN sets Ninja's flag 'restat = 1` for all action commands. This means
  that Ninja will check the timestamp of the output after the action
  completes. If output timestamp is unchanged, the step will be treated
  as if it never needed to be rebuilt, potentially eliminating some
  downstream steps for incremental builds. Scripts can improve build
  performance by taking care not to change the timstamp of the output
  file(s) if the contents have not changed.
  
  脚本将使用给定的参数执行，当前目录是根构建目录的当前目录。如果将文件传递给脚本，请参阅“gn help rebase_path”，了解如何将文件名转换为与构建目录相对应的文件名（源、输出和输入中的文件名都将被视为与当前构建文件相对应，并根据需要自动转换）。
  GN为所有动作命令设置忍者标志“restat=1”。这意味着Ninja将在操作完成后检查输出的时间戳。如果输出时间戳不变，则该步骤将被视为从未需要重新构建，从而可能消除增量构建的一些下游步骤。脚本可以通过注意在内容未更改的情况下不更改输出文件的时间戳来提高生成性能。
  
  
```

#### **File name handling**

```
  All output files must be inside the output directory of the build.
  You would generally use |$target_out_dir| or |$target_gen_dir| to
  reference the output or generated intermediate file directories,
  respectively.
  
  所有输出文件必须位于生成的输出目录中。您通常会使用|$target_out_dir|或|$target_gen_dir|分别引用输出或生成的中间文件目录。
```

#### **Variables**

```
  args, data, data_deps, depfile, deps, inputs, metadata, outputs*, pool,
  response_file_contents, script*, sources*
  * = required
```

#### **Example**

```
  # Runs the script over each IDL file. The IDL script will generate both a .cc
  # and a .h file for each input.
  action_foreach("my_idl") {
    script = "idl_processor.py"
    sources = [ "foo.idl", "bar.idl" ]

    # Our script reads this file each time, so we need to list it as a
    # dependency so we can rebuild if it changes.
    inputs = [ "my_configuration.txt" ]

    # Transformation from source file name to output file names.
    outputs = [ "$target_gen_dir/{{source_name_part}}.h",
                "$target_gen_dir/{{source_name_part}}.cc" ]

    # Note that since "args" is opaque to GN, if you specify paths here, you
    # will need to convert it to be relative to the build directory using
    # rebase_path().
    args = [
      "{{source}}",
      "-o",
      rebase_path(relative_target_gen_dir, root_build_dir) +
        "/{{source_name_part}}.h" ]
  }
```
