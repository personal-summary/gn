### gn help aliased_deps
#### **aliased_deps**: [scope] Set of crate-dependency pairs.

```
  Valid for `rust_library` targets and `executable`, `static_library`, and
  `shared_library` targets that contain Rust sources.
  对于包含rust源的“rust_library”目标和“executable”、“static_library”和“shared_library”对象有效。

  A scope, each key indicating the renamed crate and the corresponding value
  specifying the label of the dependency producing the relevant binary.
  一个范围，每个键指示重命名的板条箱，对应的值指定产生相关二进制文件的依赖项的标签。

  All dependencies listed in this field *must* be listed as deps of the target.
  此字段*中列出的所有依赖项必须*列为目标的dep。

    executable("foo") {
      sources = [ "main.rs" ]
      deps = [ "//bar" ]
    }

  This target would compile the `foo` crate with the following `extern` flag:
  `rustc ...command... --extern bar=<build_out_dir>/obj/bar`
  此目标将使用以下“extern”标志编译“foo”：“rustc…command…--extern bar=<build_out_dir>/obj/bar”`

    executable("foo") {
      sources = [ "main.rs" ]
      deps = [ ":bar" ]
      aliased_deps = {
        bar_renamed = ":bar"
      }
    }

  With the addition of `aliased_deps`, above target would instead compile with:
  `rustc ...command... --extern bar_renamed=<build_out_dir>/obj/bar`
  添加了“aliased_deps”后，上述目标将改为使用以下命令进行编译：“rustc…command…--extern bar_renamed=<build_out_dir>/obj/bar”`
```
