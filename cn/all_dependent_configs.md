### gn help all_dependent_configs
#### **all_dependent_configs**: Configs to be forced on dependents.

```
  A list of config labels.

  All targets depending on this one, and recursively, all targets depending on
  those, will have the configs listed in this variable added to them. These
  configs will also apply to the current target.

  This addition happens in a second phase once a target and all of its
  dependencies have been resolved. Therefore, a target will not see these
  force-added configs in their "configs" variable while the script is running,
  and they can not be removed. As a result, this capability should generally
  only be used to add defines and include directories necessary to compile a
  target's headers.

  See also "public_configs".
  
  配置标签列表。              
  依赖于此变量的所有目标，以及递归地依赖于这些目标的所有目标都将添加此变量中列出的配置。这些配置也将应用于当前目标。              
  一旦解决了目标及其所有依赖关系，此添加将在第二阶段进行。因此，当脚本运行时，目标不会在其“configs”变量中看到这些强制添加的配置，并且无法删除它们。因此，此功能通常只能用于添加编译目标标头所需的定义和目录。              
  另请参见“public_configs”。
```

#### **Ordering of flags and values**

```
  1. Those set on the current target (not in a config).
  2. Those set on the "configs" on the target in order that the
     configs appear in the list.
  3. Those set on the "all_dependent_configs" on the target in order
     that the configs appear in the list.
  4. Those set on the "public_configs" on the target in order that
     those configs appear in the list.
  5. all_dependent_configs pulled from dependencies, in the order of
     the "deps" list. This is done recursively. If a config appears
     more than once, only the first occurence will be used.
  6. public_configs pulled from dependencies, in the order of the
     "deps" list. If a dependency is public, they will be applied
     recursively.
     
     1.在当前目标上设置的值（不在配置中）。              
     2.在目标上的“配置”上设置的配置，以便配置出现在列表中。              
     3.在目标上的“all_dependent_configs”上设置的值，以便配置出现在列表中。              
     4.目标上“public_configs”上设置的配置，以便这些配置出现在列表中。              
     5.按照“deps”列表的顺序从依赖项中提取的all_dependent_configs。这是递归完成的。如果配置出现多次，则只使用第一次出现的配置。             
     6.按照“deps”列表的顺序从依赖项中提取public_configs。如果依赖项是公共的，则将递归地应用它们。
```
