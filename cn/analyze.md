### gn help analyze
#### **gn analyze &lt;out_dir&gt; &lt;input_path&gt; &lt;output_path&gt;**

```
  Analyze which targets are affected by a list of files.

  This command takes three arguments:

  out_dir is the path to the build directory.

  input_path is a path to a file containing a JSON object with three fields:

   - "files": A list of the filenames to check.

   - "test_targets": A list of the labels for targets that are needed to run
     the tests we wish to run.

   - "additional_compile_targets": A list of the labels for targets that we
     wish to rebuild, but aren't necessarily needed for testing. The important
     difference between this field and "test_targets" is that if an item in
     the additional_compile_targets list refers to a group, then any
     dependencies of that group will be returned if they are out of date, but
     the group itself does not need to be. If the dependencies themselves are
     groups, the same filtering is repeated. This filtering can be used to
     avoid rebuilding dependencies of a group that are unaffected by the input
     files. The list may also contain the string "all" to refer to a
     pseudo-group that contains every root target in the build graph.

     This filtering behavior is also known as "pruning" the list of compile
     targets.

  If input_path is -, input is read from stdin.

  output_path is a path indicating where the results of the command are to be
  written. The results will be a file containing a JSON object with one or more
  of following fields:

   - "compile_targets": A list of the labels derived from the input
     compile_targets list that are affected by the input files. Due to the way
     the filtering works for compile targets as described above, this list may
     contain targets that do not appear in the input list.

   - "test_targets": A list of the labels from the input test_targets list that
     are affected by the input files. This list will be a proper subset of the
     input list.

   - "invalid_targets": A list of any names from the input that do not exist in
     the build graph. If this list is non-empty, the "error" field will also be
     set to "Invalid targets".

   - "status": A string containing one of three values:

       - "Found dependency"
       - "No dependency"
       - "Found dependency (all) "

     In the first case, the lists returned in compile_targets and test_targets
     should be passed to ninja to build. In the second case, nothing was
     affected and no build is necessary. In the third case, GN could not
     determine the correct answer and returned the input as the output in order
     to be safe.

   - "error": This will only be present if an error occurred, and will contain
     a string describing the error. This includes cases where the input file is
     not in the right format, or contains invalid targets.

  If output_path is -, output is written to stdout.

  The command returns 1 if it is unable to read the input file or write the
  output file, or if there is something wrong with the build such that gen
  would also fail, and 0 otherwise. In particular, it returns 0 even if the
  "error" key is non-empty and a non-fatal error occurred. In other words, it
  tries really hard to always write something to the output JSON and convey
  errors that way rather than via return codes.
  
  分析受文件列表影响的目标。
  此命令包含三个参数：              
  out_dir是构建目录的路径。              
  input_path是包含三个字段的JSON对象的文件的路径：              
  -“files”：要检查的文件名列表。              
  -“test_targets”：需要运行的目标的标签列表              
  我们希望运行的测试。              
  -“additional_compile_targets”：我们希望重建但不一定需要测试的目标的标签列表。此字段与“test_targets”之间的重要区别在于，如果additional_compile_targets列表中的某个项引用了一个组，则如果该组的任何依赖项已过期，则将返回该组的所有依赖项，但该组本身不必如此。如果依赖项本身是组，则重复相同的筛选。此筛选可用于避免重建不受输入文件影响的组的依赖关系。该列表还可以包含字符串“all”，以引用包含构建图中每个根目标的伪组。
  这种过滤行为也称为“修剪”编译列表   
  目标。              
  如果input_path为-，则从stdin读取输入。              
  output_path是一个路径，指示命令结果要写入的位置。结果将是一个包含JSON对象的文件，该对象包含以下一个或多个字段：             
  -“compile_targets”：从输入compile_tTargets列表派生的受输入文件影响的标签列表。由于过滤对编译目标的工作方式如上所述，此列表可能包含未出现在输入列表中的目标。              
  -“test_targets”：输入测试目标列表中受输入文件影响的标签列表。该列表将是输入列表的适当子集。              
  -“invalid_targets”：输入中不存在于构建图中的任何名称的列表。如果此列表为非空，则“错误”字段也将设置为“无效目标”。              
  -“status”：包含以下三个值之一的字符串：              
  -“找到依赖项”              
  -“无依赖关系”              
  -“找到依赖项（全部）”              
  在第一种情况下，compile_targets和test_targets中返回的列表应该传递给ninja进行构建。在第二种情况下，没有任何影响，不需要构建。在第三种情况下，GN无法确定正确答案，为了安全起见，将输入作为输出返回。             
  -“error”：只有在发生错误时才会出现，并包含描述错误的字符串。这包括输入文件格式不正确或包含无效目标的情况。              
  如果output_path为-，则输出将写入stdout。              
  如果该命令无法读取输入文件或写入输出文件，或者如果构建有问题，因此gen也会失败，则返回1，否则返回0。特别是，即使“error”键为非空并且发生了非致命错误，它也会返回0。换句话说，它非常努力地总是向输出JSON写一些东西，并通过这种方式而不是通过返回代码传递错误。
  
```

