### gn help assert
#### **assert**: Assert an expression is true at generation time.

```
  assert(<condition> [, <error string>])

  If the condition is false, the build will fail with an error. If the
  optional second argument is provided, that string will be printed
  with the error message.
```

#### **Examples**

```
  assert(is_win)
  assert(defined(sources), "Sources must be defined");
```
