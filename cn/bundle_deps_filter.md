### gn help bundle_deps_filter
#### **bundle_deps_filter**: [label list] A list of labels that are filtered out.

```
  A list of target labels.

  This list contains target label patterns that should be filtered out when
  creating the bundle. Any target matching one of those label will be removed
  from the dependencies of the create_bundle target.

  This is mostly useful when creating application extension bundle as the
  application extension has access to runtime resources from the application
  bundle and thus do not require a second copy.

  See "gn help create_bundle" for more information.
```

#### **Example**

```
  create_bundle("today_extension") {
    deps = [
      "//base"
    ]
    bundle_root_dir = "$root_out_dir/today_extension.appex"
    bundle_deps_filter = [
      # The extension uses //base but does not use any function calling into
      # third_party/icu and thus does not need the icudtl.dat file.
      "//third_party/icu:icudata",
    ]
  }
```
