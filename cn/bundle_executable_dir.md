### gn help bundle_executable_dir
#### **bundle_executable_dir**

```
  bundle_executable_dir: Expansion of {{bundle_executable_dir}} in
                         create_bundle.

  A string corresponding to a path in $root_build_dir.

  This string is used by the "create_bundle" target to expand the
  {{bundle_executable_dir}} of the "bundle_data" target it depends on. This
  must correspond to a path under "bundle_root_dir".

  See "gn help bundle_root_dir" for examples.
```
