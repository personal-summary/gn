### gn help code_signing_args
#### **code_signing_args**: [string list] Arguments passed to code signing script.

```
  For create_bundle targets, code_signing_args is the list of arguments to pass
  to the code signing script. Typically you would use source expansion (see "gn
  help source_expansion") to insert the source file names.

  See also "gn help create_bundle".
```
