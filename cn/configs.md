### gn help configs
#### **configs**: Configs applying to this target or config.

```
  A list of config labels.
  配置标签列表。
```

#### **Configs on a target**

```
  When used on a target, the include_dirs, defines, etc. in each config are
  appended in the order they appear to the compile command for each file in the
  target. They will appear after the include_dirs, defines, etc. that the
  target sets directly.

  Since configs apply after the values set on a target, directly setting a
  compiler flag will prepend it to the command line. If you want to append a
  flag instead, you can put that flag in a one-off config and append that
  config to the target's configs list.

  The build configuration script will generally set up the default configs
  applying to a given target type (see "set_defaults"). When a target is being
  defined, it can add to or remove from this list.
  
  当在目标上使用时，每个配置中的include_dir、defines等将按照它们出现在目标中每个文件的编译命令中的顺序追加。它们将出现在目标直接设置的include_dir、define等之后。              
  由于在目标上设置的值之后应用配置，因此直接设置编译器标志将在命令行之前添加它。如果您想附加一个标志，可以将该标志放在一次性配置中，然后将该配置附加到目标的配置列表中。              
  构建配置脚本通常会设置应用于给定目标类型的默认配置（请参阅“set_defaults”）。定义目标时，可以将其添加到此列表或从列表中删除。
```

#### **Configs on a config**

```
  It is possible to create composite configs by specifying configs on a config.
  One might do this to forward values, or to factor out blocks of settings from
  very large configs into more manageable named chunks.

  In this case, the composite config is expanded to be the concatenation of its
  own values, and in order, the values from its sub-configs *before* anything
  else happens. This has some ramifications:

   - A target has no visibility into a config's sub-configs. Target code only
     sees the name of the composite config. It can't remove sub-configs or opt
     in to only parts of it. The composite config may not even be defined
     before the target is.

   - You can get duplication of values if a config is listed twice, say, on a
     target and in a sub-config that also applies. In other cases, the configs
     applying to a target are de-duped. It's expected that if a config is
     listed as a sub-config that it is only used in that context. (Note that
     it's possible to fix this and de-dupe, but it's not normally relevant and
     complicates the implementation.)
     
     通过在配置上指定配置，可以创建复合配置。可以这样做来转发值，或者将非常大的配置中的设置块分解为更易于管理的命名块。              
     在这种情况下，复合配置被扩展为其自身值的串联，并且按照顺序，在发生任何其他事情之前，其子配置中的值*。这会产生一些后果：              
     -目标对配置的子配置没有可见性。目标代码只能看到复合配置的名称。它不能删除子配置或只选择其中的一部分。复合配置甚至可能在目标之前都没有定义。                -如果一个配置被列出两次，例如，在一个目标上和一个同样适用的子配置中，您可以得到重复的值。在其他情况下，将对应用于目标的配置进行重复数据消除。如果一个配置被列为子配置，那么它只在该上下文中使用。（请注意，可以修复此问题并进行重复数据消除，但这通常不相关，而且会使实施变得复杂。）
```

#### **Ordering of flags and values**

```
  1. Those set on the current target (not in a config).
  2. Those set on the "configs" on the target in order that the
     configs appear in the list.
  3. Those set on the "all_dependent_configs" on the target in order
     that the configs appear in the list.
  4. Those set on the "public_configs" on the target in order that
     those configs appear in the list.
  5. all_dependent_configs pulled from dependencies, in the order of
     the "deps" list. This is done recursively. If a config appears
     more than once, only the first occurence will be used.
  6. public_configs pulled from dependencies, in the order of the
     "deps" list. If a dependency is public, they will be applied
     recursively.
     
     1.在当前目标上设置的值（不在配置中）。              
     2.在目标上的“配置”上设置的配置，以便配置出现在列表中。              
     3.在目标上的“all_dependent_configs”上设置的值，以便配置出现在列表中。              
     4.目标上“public_configs”上设置的配置，以便这些配置出现在列表中。              
     5.按照“deps”列表的顺序从依赖项中提取的all_dependent_configs。这是递归完成的。如果配置出现多次，则只使用第一次出现的配置。              
     6.按照“deps”列表的顺序从依赖项中提取public_configs。如果依赖项是公共的，则将递归地应用它们。
```

#### **Example**

```
  # Configs on a target.
  source_set("foo") {
    # Don't use the default RTTI config that BUILDCONFIG applied to us.
    configs -= [ "//build:no_rtti" ]

    # Add some of our own settings.
    configs += [ ":mysettings" ]
  }

  # Create a default_optimization config that forwards to one of a set of more
  # specialized configs depending on build flags. This pattern is useful
  # because it allows a target to opt in to either a default set, or a more
  # specific set, while avoid duplicating the settings in two places.
  config("super_optimization") {
    cflags = [ ... ]
  }
  config("default_optimization") {
    if (optimize_everything) {
      configs = [ ":super_optimization" ]
    } else {
      configs = [ ":no_optimization" ]
    }
  }
```
