### gn help copy
#### **copy**: Declare a target that copies files.

#### **File name handling**

参考文档：[gn help source_expansion](./source_expansion.md)

```
  所有的输出文件都必须在构建输出目录里面。通常会使用到$target_out_dir或者$target_gen_dir两个变量来指定目标的输出文件目录以及生成中间文件目录。
  
  必须同时指定"sources" and "outputs"两个变量。Sources变量可以包含你愿意包含的所有文件，但是outputs变量必须只有一个。

  如果源文件不止一个，那么输出文件名中就要使用到源扩展，关于源扩展可以参考"gn help source_expansion"。
  例如可以使用类似于"{{source_name_part}}"这样的占位符。
```

#### **Examples**

```
  # Write a rule that copies a checked-in DLL to the output directory.
  copy("mydll") {
    sources = [ "mydll.dll" ]
    outputs = [ "$target_out_dir/mydll.dll" ]
  }

  # Write a rule to copy several files to the target generated files directory.
  copy("myfiles") {
    sources = [ "data1.dat", "data2.dat", "data3.dat" ]

    # Use source expansion to generate output files with the corresponding file
    # names in the gen dir. This will just copy each file.
    outputs = [ "$target_gen_dir/{{source_file_part}}" ]
  }
```
