### gn help crate_name
#### **crate_name**: [string] The name for the compiled crate.

```
  Valid for `rust_library` targets and `executable`, `static_library`,
  `shared_library`, and `source_set` targets that contain Rust sources.

  If crate_name is not set, then this rule will use the target name.
```
