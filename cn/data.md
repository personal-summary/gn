### gn help data
#### **data**: Runtime data file dependencies.

```
  Lists files or directories required to run the given target. These are
  typically data files or directories of data files. The paths are interpreted
  as being relative to the current build file. Since these are runtime
  dependencies, they do not affect which targets are built or when. To declare
  input files to a script, use "inputs".

  Appearing in the "data" section does not imply any special handling such as
  copying them to the output directory. This is just used for declaring runtime
  dependencies. Runtime dependencies can be queried using the "runtime_deps"
  category of "gn desc" or written during build generation via
  "--runtime-deps-list-file".

  GN doesn't require data files to exist at build-time. So actions that produce
  files that are in turn runtime dependencies can list those generated files
  both in the "outputs" list as well as the "data" list.

  By convention, directories are listed with a trailing slash:
    data = [ "test/data/" ]
  However, no verification is done on these so GN doesn't enforce this. The
  paths are just rebased and passed along when requested.

  Note: On iOS and macOS, create_bundle targets will not be recursed into when
  gathering data. See "gn help create_bundle" for details.

  See "gn help runtime_deps" for how these are used.
```
