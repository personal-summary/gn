### gn help data_deps
#### **data_deps**: Non-linked dependencies.

```
  A list of target labels.

  Specifies dependencies of a target that are not actually linked into the
  current target. Such dependencies will be built and will be available at
  runtime.

  This is normally used for things like plugins or helper programs that a
  target needs at runtime.

  Note: On iOS and macOS, create_bundle targets will not be recursed into when
  gathering data_deps. See "gn help create_bundle" for details.

  See also "gn help deps" and "gn help data".
```

#### **Example**

```
  executable("foo") {
    deps = [ "//base" ]
    data_deps = [ "//plugins:my_runtime_plugin" ]
  }
```
