### gn help data_keys
#### **data_keys**: Keys from which to collect metadata.

```
  These keys are used to identify metadata to collect. If a walked target
  defines this key in its metadata, its value will be appended to the resulting
  collection.

  See "gn help generated_file".
```
