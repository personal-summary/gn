### gn help default_toolchain
#### **default_toolchain**: [string] Label of the default toolchain.

```
  A fully-qualified label representing the default toolchain, which may not
  necessarily be the current one (see "current_toolchain").
```
