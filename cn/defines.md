### gn help defines
#### **defines**: C preprocessor defines.

```
  A list of strings

  These strings will be passed to the C/C++ compiler as #defines. The strings
  may or may not include an "=" to assign a value.
```

#### **Ordering of flags and values**

```
  1. Those set on the current target (not in a config).
  2. Those set on the "configs" on the target in order that the
     configs appear in the list.
  3. Those set on the "all_dependent_configs" on the target in order
     that the configs appear in the list.
  4. Those set on the "public_configs" on the target in order that
     those configs appear in the list.
  5. all_dependent_configs pulled from dependencies, in the order of
     the "deps" list. This is done recursively. If a config appears
     more than once, only the first occurence will be used.
  6. public_configs pulled from dependencies, in the order of the
     "deps" list. If a dependency is public, they will be applied
     recursively.
```

#### **Example**

```
  defines = [ "AWESOME_FEATURE", "LOG_LEVEL=3" ]
```
