### gn help depfile
#### **depfile**: [string] File name for input dependencies for actions.

```
  If nonempty, this string specifies that the current action or action_foreach
  target will generate the given ".d" file containing the dependencies of the
  input. Empty or unset means that the script doesn't generate the files.

  A depfile should be used only when a target depends on files that are not
  already specified by a target's inputs and sources. Likewise, depfiles should
  specify only those dependencies not already included in sources or inputs.

  The .d file should go in the target output directory. If you have more than
  one source file that the script is being run over, you can use the output
  file expansions described in "gn help action_foreach" to name the .d file
  according to the input."

  The format is that of a Makefile and all paths must be relative to the root
  build directory. Only one output may be listed and it must match the first
  output of the action.

  Although depfiles are created by an action, they should not be listed in the
  action's "outputs" unless another target will use the file as an input.
```

#### **Example**

```
  action_foreach("myscript_target") {
    script = "myscript.py"
    sources = [ ... ]

    # Locate the depfile in the output directory named like the
    # inputs but with a ".d" appended.
    depfile = "$relative_target_output_dir/{{source_name}}.d"

    # Say our script uses "-o <d file>" to indicate the depfile.
    args = [ "{{source}}", "-o", depfile ]
  }
```
