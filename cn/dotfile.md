### gn help dotfile
#### **.gn file**

```
  When gn starts, it will search the current directory and parent directories
  for a file called ".gn". This indicates the source root. You can override
  this detection by using the --root command-line argument

  The .gn file in the source root will be executed. The syntax is the same as a
  buildfile, but with very limited build setup-specific meaning.

  If you specify --root, by default GN will look for the file .gn in that
  directory. If you want to specify a different file, you can additionally pass
  --dotfile:

    gn gen out/Debug --root=/home/build --dotfile=/home/my_gn_file.gn
    当gn启动时，它将在当前目录和父目录中搜索名为“.gn”的文件。这表示源根。您可以使用--root命令行参数覆盖此检测              
    将执行源根目录中的.gn文件。语法与构建文件相同，但具有非常有限的构建设置特定含义。              
    如果指定--root，默认情况下GN将在该目录中查找文件.GN。如果要指定不同的文件，还可以通过              
    --点文件：              
    gn-gen-out/Debug--root=/home/build--dotfile=/home/my_gn_file.gn
```

#### **Variables**

```
  arg_file_template [optional]
      Path to a file containing the text that should be used as the default
      args.gn content when you run `gn args`.
      包含应用作默认参数的文本的文件的路径。
      运行“gn args”时的gn内容。

  buildconfig [required]
      Path to the build config file. This file will be used to set up the
      build file execution environment for each toolchain.
      生成配置文件的路径。此文件将用于为每个工具链设置构建文件执行环境。

  check_targets [optional]
      A list of labels and label patterns that should be checked when running
      "gn check" or "gn gen --check". If unspecified, all targets will be
      checked. If it is the empty list, no targets will be checked. To
      bypass this list, request an explicit check of targets, like "//*".

      The format of this list is identical to that of "visibility" so see "gn
      help visibility" for examples.
      运行“gn check”或“gn gen-check”时应检查的标签和标签模式列表。如果未指定，将检查所有目标。如果是空列表，则不会检查任何目标。要绕过此列表，请请求明确检查目标，如“//*”。              
      此列表的格式与“可见性”的格式相同，因此请参见“gn help visibility”以获取示例。

  check_system_includes [optional]
      Boolean to control whether system style includes are checked by default
      when running "gn check" or "gn gen --check".  System style includes are
      includes that use angle brackets <> instead of double quotes "". If this
      setting is omitted or set to false, these includes will be ignored by
      default. They can be checked explicitly by running
      "gn check --check-system" or "gn gen --check=system"
      布尔值，用于控制在运行“gn check”或“gn gen-check”时是否默认选中系统样式包含。系统样式包含使用尖括号<>而不是双引号“”的包含。如果忽略此设置或将其设置为false，则默认情况下将忽略这些includes。可以通过运行              
      “gn check--check system”或“gn gen--check=system”

  exec_script_whitelist [optional]
      A list of .gn/.gni files (not labels) that have permission to call the
      exec_script function. If this list is defined, calls to exec_script will
      be checked against this list and GN will fail if the current file isn't
      in the list.

      This is to allow the use of exec_script to be restricted since is easy to
      use inappropriately. Wildcards are not supported. Files in the
      secondary_source tree (if defined) should be referenced by ignoring the
      secondary tree and naming them as if they are in the main tree.

      If unspecified, the ability to call exec_script is unrestricted.
      .gn/的列表。具有调用execscript函数权限的gni文件（不是标签）。如果定义了此列表，将根据此列表检查对exec_script的调用，如果当前文件不在列表中，GN将失败。              
      这是为了限制exec_script的使用，因为它很容易使用不当。不支持通配符。应通过忽略辅助树并将其命名为与主树中的文件一样来引用辅助源树（如果已定义）中的文件。              
      如果未指定，则调用exec_script的能力不受限制。

      Example:
        exec_script_whitelist = [
          "//base/BUILD.gn",
          "//build/my_config.gni",
        ]

  root [optional]
      Label of the root build target. The GN build will start by loading the
      build file containing this target name. This defaults to "//:" which will
      cause the file //BUILD.gn to be loaded.
      根生成目标的标签。GN构建将从加载包含此目标名称的构建文件开始。默认为“//：”，这将导致文件//BUILD。gn将被加载。

  script_executable [optional]
      Path to specific Python executable or other interpreter to use in
      action targets and exec_script calls. By default GN searches the
      PATH for Python to execute these scripts.

      If set to the empty string, the path specified in action targets
      and exec_script calls will be executed directly.
      要在操作目标和exec_script调用中使用的特定Python可执行文件或其他解释器的路径。默认情况下，GN在PATH中搜索Python以执行这些脚本。              
      如果设置为空字符串，将直接执行操作目标和exec_script调用中指定的路径。

  secondary_source [optional]
      Label of an alternate directory tree to find input files. When searching
      for a BUILD.gn file (or the build config file discussed above), the file
      will first be looked for in the source root. If it's not found, the
      secondary source root will be checked (which would contain a parallel
      directory hierarchy).

      This behavior is intended to be used when BUILD.gn files can't be checked
      in to certain source directories for whatever reason.

      The secondary source root must be inside the main source tree.
      用于查找输入文件的备用目录树的标签。搜索BUILD时。gn文件（或上面讨论的构建配置文件），将首先在源根目录中查找该文件。如果找不到，将检查辅助源根目录（它将包含并行目录层次结构）。              
      此行为旨在在BUILD时使用。无论出于何种原因，gn文件都无法签入某些源目录。              
      辅助源根必须位于主源树中。

  default_args [optional]
      Scope containing the default overrides for declared arguments. These
      overrides take precedence over the default values specified in the
      declare_args() block, but can be overriden using --args or the
      args.gn file.

      This is intended to be used when subprojects declare arguments with
      default values that need to be changed for whatever reason.
      包含声明参数的默认重写的作用域。这些重写优先于declare_args（）块中指定的默认值，但可以使用--args或args重写。gn文件。              
      当子项目声明具有默认值的参数时，无论出于何种原因，都需要更改这些参数，这将被使用。
```

#### **Example .gn file contents**

```
  buildconfig = "//build/config/BUILDCONFIG.gn"

  check_targets = [
    "//doom_melon/*",  # Check everything in this subtree.
    "//tools:mind_controlling_ant",  # Check this specific target.
  ]

  root = "//:root"

  secondary_source = "//build/config/temporary_buildfiles/"

  default_args = {
    # Default to release builds for this project.
    is_debug = false
    is_component_build = false
  }
```
