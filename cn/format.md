### gn help format
#### **gn format [\--dump-tree] (\--stdin | &lt;list of build_files...&gt;)**

```
  格式化gn文件为标准格式。例如gn文件的空格、换行不标准，通过"gn format"后格式化成标准格式。
  
  
  'sources', 'deps'等列表内容会被按照规范顺序进行排序。如果不想进行格式化操作，你可以在'sources', 'deps'等列表前面添加"# NOSORT"注释。
  例如：
  # NOSORT
  sources = [
    "z.cc",
    "a.cc",
  ]
```

#### **Arguments**

```
  --dry-run
      不修改和输出任何内容，但是会根据输出是否与磁盘上的内容有不同来设置进程的退出代码。这会在presubmit/lint-type检查时候使用。
      - Exit code 0: successful format, matches on disk.
      - Exit code 1: general failure (parse error, etc.)
      - Exit code 2: successful format, but differs from on disk.

  --dump-tree[=( text | json )]
      将parse tree输出到stdout上，并且不会更新文件以及打印格式化输出。如果未指定格式，将使用文本格式。

  --stdin
      格式化的文件需要从 stdin 中读取。
```

#### **Examples**
```
  gn format //some/BUILD.gn //some/other/BUILD.gn //and/another/BUILD.gn
  gn format some\\BUILD.gn
  gn format /abspath/some/BUILD.gn
  gn format --stdin
```
