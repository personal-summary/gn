### gn help friend
#### **friend**: Allow targets to include private headers.

```
  A list of label patterns (see "gn help label_pattern") that allow dependent
  targets to include private headers. Applies to all binary targets.

  Normally if a target lists headers in the "public" list (see "gn help
  public"), other headers are implicitly marked as private. Private headers
  can not be included by other targets, even with a public dependency path.
  The "gn check" function performs this validation.

  A friend declaration allows one or more targets to include private headers.
  This is useful for things like unit tests that are closely associated with a
  target and require internal knowledge without opening up all headers to be
  included by all dependents.

  A friend target does not allow that target to include headers when no
  dependency exists. A public dependency path must still exist between two
  targets to include any headers from a destination target. The friend
  annotation merely allows the use of headers that would otherwise be
  prohibited because they are private.

  The friend annotation is matched only against the target containing the file
  with the include directive. Friend annotations are not propagated across
  public or private dependencies. Friend annotations do not affect visibility.
```

#### **Example**

```
  static_library("lib") {
    # This target can include our private headers.
    friend = [ ":unit_tests" ]

    public = [
      "public_api.h",  # Normal public API for dependent targets.
    ]

    # Private API and sources.
    sources = [
      "a_source_file.cc",

      # Normal targets that depend on this one won't be able to include this
      # because this target defines a list of "public" headers. Without the
      # "public" list, all headers are implicitly public.
      "private_api.h",
    ]
  }

  executable("unit_tests") {
    sources = [
      # This can include "private_api.h" from the :lib target because it
      # depends on that target and because of the friend annotation.
      "my_test.cc",
    ]

    deps = [
      ":lib",  # Required for the include to be allowed.
    ]
  }
```
