### gn help gen
#### **gn gen [\--check] [&lt;ide options&gt;] &lt;out_dir&gt;**

```
  通过current tree生成ninja文件，同时将他们放到指定的输出目录output directory中。
  
  输出路径output directory可以是基于源代码存储库的绝对路径source-repo-absolute，例如：//out/foo
  也可以是基于当前路径的相对路径，例如：../../out/foo
  意思就是如果以绝对路径表示则一定以//开头，且根目录是源代码仓的根目录
  如果以相对路径表示则是基于当前文件的文件。
  
  命令"gn gen --check"等同于"gn check"，也就是说"gn gen --check=system"命令和"gn check --check-system"命令是一致的
  关于check的更对内容可以参见"gn help check"
  常用命令行的控制开关可以参考"gn help switches"
```

#### **IDE options**

```
  GN optionally generates files for IDE. Possibilities for <ide options>
  GN可以根据不同IDE生成对应文件，IDE的可能选项如下：

  --ide=<ide_name>
      ide_name当前支持以下选项：
      "eclipse" - Eclipse CDT settings file.
      "vs" - Visual Studio project/solution files.
             (default Visual Studio version: 2019)
      "vs2013" - Visual Studio 2013 project/solution files.
      "vs2015" - Visual Studio 2015 project/solution files.
      "vs2017" - Visual Studio 2017 project/solution files.
      "vs2019" - Visual Studio 2019 project/solution files.
      "xcode" - Xcode workspace/solution files.
      "qtcreator" - QtCreator project files.
      "json" - JSON file containing target information

  --filters=<path_prefixes>
      用于限制生成projects的label patterns列表内容之间使用分号作为分隔符，可参考"gn help label_pattern"。
      通过filters选项使得解决方案中仅包含需要的目标和他们的依赖项。当前该选项只在Visual Studio, Xcode 和JSON中可以使用。
```

#### **Visual Studio Flags**

```
  --sln=<file_name>
      覆盖默认的sln文件名（Solution file）。sln文件是要被写入到编译跟目录下。

  --no-deps
      解决方案中去除--no-deps指定的依赖项。用于修改--filters选项的方法。--no-deps的目标只能是已经前前面被包含了的。

  --winsdk=<sdk_version>
      使用指定的win10 sdk版本去生成项目文件
      例如可以指定的"10.0.15063.0"sdk版本替代默认版本去更新SDK。

  --ninja-extra-args=<string>
      该参数字符串可以在没有使用任何引号的情况下传递给ninja的命令行，用作ninja的参数配置，例如 -j。
```

#### **Xcode Flags**

```
  --workspace=<file_name>
      覆盖默认的workspace文件名。workspace文件是要被写入到编译跟目录下。

  --ninja-executable=<string>
      用于指定构建时要使用的 ninja 可执行文件。

  --ninja-extra-args=<string>
      该参数字符串可以在没有使用任何引号的情况下传递给ninja的命令行，用作ninja的参数配置，例如 -j。

  --root-target=<target_name>
      Xcode中"All"目标对应的目标名称。如果没有设置，”All"调用没有任何目标的ninja，然后全版本编译。
```

#### **QtCreator Flags**

```
  --root-target=<target_name>
      Name of the root target for which the QtCreator project will be generated
      to contain files of it and its dependencies. If unset, the whole build
      graph will be emitted.
```

#### **Eclipse IDE Support**

```
  GN DOES NOT generate Eclipse CDT projects. Instead, it generates a settings
  file which can be imported into an Eclipse CDT project. The XML file contains
  a list of include paths and defines. Because GN does not generate a full
  .cproject definition, it is not possible to properly define includes/defines
  for each file individually. Instead, one set of includes/defines is generated
  for the entire project. This works fairly well but may still result in a few
  indexer issues here and there.
```

#### **Generic JSON Output**

```
  Dumps target information to a JSON file and optionally invokes a
  python script on the generated file. See the comments at the beginning
  of json_project_writer.cc and desc_builder.cc for an overview of the JSON
  file format.

  --json-file-name=<json_file_name>
      Overrides default file name (project.json) of generated JSON file.

  --json-ide-script=<path_to_python_script>
      Executes python script after the JSON file is generated. Path can be
      project absolute (//), system absolute (/) or relative, in which case the
      output directory will be base. Path to generated JSON file will be first
      argument when invoking script.

  --json-ide-script-args=<argument>
      Optional second argument that will passed to executed script.
```

#### **Compilation Database**

```
  --export-compile-commands[=<target_name1,target_name2...>]
      Produces a compile_commands.json file in the root of the build directory
      containing an array of “command objects”, where each command object
      specifies one way a translation unit is compiled in the project. If a list
      of target_name is supplied, only targets that are reachable from the list
      of target_name will be used for “command objects” generation, otherwise
      all available targets will be used. This is used for various Clang-based
      tooling, allowing for the replay of individual compilations independent
      of the build system.
```
