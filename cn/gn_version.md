### gn help gn_version
#### **gn_version**: [number] The version of gn.

```
  Corresponds to the number printed by `gn --version`.
```

#### **Example**

```
  assert(gn_version >= 1700, "need GN version 1700 for the frobulate feature")
```
