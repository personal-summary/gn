### gn help help
#### **gn help &lt;anything&gt;**

```
知道大家喜欢帮助文档，所以就把这些命令的相关信息放在帮助文档上了。
大家可以使用gn help all一次性获取所有的帮助信息。
```

#### **Switches**

```
  --markdown
      大家可以添加--markdown参数使得命令能以MarkDown格式输出。
```

#### **Example**

```
  gn help --markdown all
      所有的帮助文档内容以Markdown格式输出出来
```
