### gn help host_cpu
#### **host_cpu**: The processor architecture that GN is running on.

```
  This is value is exposed so that cross-compile toolchains can access the host
  architecture when needed.

  The value should generally be considered read-only, but it can be overriden
  in order to handle unusual cases where there might be multiple plausible
  values for the host architecture (e.g., if you can do either 32-bit or 64-bit
  builds). The value is not used internally by GN for any purpose.
```

#### **Some possible values**

```
  - "x64"
  - "x86"
```
