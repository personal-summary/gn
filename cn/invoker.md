### gn help invoker
#### **invoker**: [string] The invoking scope inside a template.

```
  Inside a template invocation, this variable refers to the scope of the
  invoker of the template. Outside of template invocations, this variable is
  undefined.

  All of the variables defined inside the template invocation are accessible as
  members of the "invoker" scope. This is the way that templates read values
  set by the callers.

  This is often used with "defined" to see if a value is set on the invoking
  scope.

  See "gn help template" for more examples.
```

#### **Example**

```
  template("my_template") {
    print(invoker.sources)       # Prints [ "a.cc", "b.cc" ]
    print(defined(invoker.foo))  # Prints false.
    print(defined(invoker.bar))  # Prints true.
  }

  my_template("doom_melon") {
    sources = [ "a.cc", "b.cc" ]
    bar = 123
  }
```
