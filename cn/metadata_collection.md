### gn help metadata_collection
#### **Metadata Collection**

```
  Metadata is information attached to targets throughout the dependency tree. GN
  allows for the collection of this data into files written during the generation
  step, enabling users to expose and aggregate this data based on the dependency
  tree.
  元数据是整个依赖关系树中附加到目标的信息。GN允许将这些数据收集到生成步骤中编写的文件中，使用户能够基于依赖关系树公开和聚合这些数据。
```

#### **generated_file targets**

```
  Similar to the write_file() function, the generated_file target type
  creates a file in the specified location with the specified content. The
  primary difference between write_file() and this target type is that the
  write_file function does the file write at parse time, while the
  generated_file target type writes at target resolution time. See
  "gn help generated_file" for more detail.

  When written at target resolution time, generated_file enables GN to
  collect and write aggregated metadata from dependents.

  A generated_file target can declare either 'contents' to write statically
  known contents to a file or 'data_keys' to aggregate metadata and write the
  result to a file. It can also specify 'walk_keys' (to restrict the metadata
  collection), 'output_conversion', and 'rebase'.
  
  与write_file（）函数类似，generated_file目标类型在指定位置创建具有指定内容的文件。write_file（）与此目标类型之间的主要区别在于write_file函数在解析时执行文件写入，而generated_file目标类型在目标解析时执行写入。有关详细信息，请参阅“gn help generated_file”。              
  在目标解析时间写入时，generated_file使GN能够从从属项收集和写入聚合元数据。              
  generated_file目标可以声明“contents”以将静态已知内容写入文件，也可以声明“data_keys”以聚合元数据并将结果写入文件。它还可以指定“walk_keys”（以限制元数据集合）、“output_convertion”和“rebase”。
```

#### **Collection and Aggregation**

```
  Targets can declare a 'metadata' variable containing a scope, and this
  metadata may be collected and written out to a file specified by
  generated_file aggregation targets. The 'metadata' scope must contain
  only list values since the aggregation step collects a list of these values.

  During the target resolution, generated_file targets will walk their
  dependencies recursively, collecting metadata based on the specified
  'data_keys'. 'data_keys' is specified as a list of strings, used by the walk
  to identify which variables in dependencies' 'metadata' scopes to collect.

  The walk begins with the listed dependencies of the 'generated_file' target.
  The 'metadata' scope for each dependency is inspected for matching elements
  of the 'generated_file' target's 'data_keys' list.  If a match is found, the
  data from the dependent's matching key list is appended to the aggregate walk
  list. Note that this means that if more than one walk key is specified, the
  data in all of them will be aggregated into one list. From there, the walk
  will then recurse into the dependencies of each target it encounters,
  collecting the specified metadata for each.
  目标可以声明包含范围的“元数据”变量，并且可以收集此元数据并将其写入generated_file聚合目标指定的文件。“metadata”范围只能包含列表值，因为聚合步骤收集这些值的列表。              
  在目标解析过程中，generated_file目标将递归地遍历其依赖项，根据指定的“data_keys”收集元数据datakeys被指定为字符串列表，walk使用它来标识要收集的依赖项“元数据”范围中的哪些变量。              
  步行从“generated_file”目标的列出依赖项开始。检查每个依赖项的“元数据”范围，以查找“generated_file”目标的“data_keys”列表中的匹配元素。如果找到匹配项，则依赖项的匹配关键字列表中的数据将附加到聚合遍历列表中。注意，这意味着如果指定了多个walk键，则所有这些键中的数据都将聚合到一个列表中。从那里开始，遍历将递归到它遇到的每个目标的依赖关系中，为每个目标收集指定的元数据。

  For example:

    group("a") {
      metadata = {
        doom_melon = [ "enable" ]
        my_files = [ "foo.cpp" ]
        my_extra_files = [ "bar.cpp" ]
      }

      deps = [ ":b" ]
    }

    group("b") {
      metadata = {
        my_files = [ "baz.cpp" ]
      }
    }

    generated_file("metadata") {
      outputs = [ "$root_build_dir/my_files.json" ]
      data_keys = [ "my_files", "my_extra_files" ]

      deps = [ ":a" ]
    }

  The above will produce the following file data:

    foo.cpp
    bar.cpp
    baz.cpp

  The dependency walk can be limited by using the 'walk_keys'. This is a list of
  labels that should be included in the walk. All labels specified here should
  also be in one of the deps lists. These keys act as barriers, where the walk
  will only recurse into the targets listed. An empty list in all specified
  barriers will end that portion of the walk.
  可以使用“walk_keys”来限制依赖关系漫游。这是行走中应包含的标签列表。此处指定的所有标签也应在其中一个deps列表中。这些关键点充当屏障，在那里行走只会循环到列出的目标中。所有指定障碍中的空列表将结束该部分步行。

    group("a") {
      metadata = {
        my_files = [ "foo.cpp" ]
        my_files_barrier [ ":b" ]
      }

      deps = [ ":b", ":c" ]
    }

    group("b") {
      metadata = {
        my_files = [ "bar.cpp" ]
      }
    }

    group("c") {
      metadata = {
        my_files = [ "doom_melon.cpp" ]
      }
    }

    generated_file("metadata") {
      outputs = [ "$root_build_dir/my_files.json" ]
      data_keys = [ "my_files", "my_extra_files" ]

      deps = [ ":a" ]
    }

  The above will produce the following file data (note that `doom_melon.cpp` is
  not included):

    foo.cpp
    bar.cpp

  A common example of this sort of barrier is in builds that have host tools
  built as part of the tree, but do not want the metadata from those host tools
  to be collected with the target-side code.
  这类障碍的一个常见示例是在构建时将宿主工具构建为树的一部分，但不希望这些宿主工具的元数据与目标端代码一起收集。
```

#### **Common Uses**

```
  Metadata can be used to collect information about the different targets in the
  build, and so a common use is to provide post-build tooling with a set of data
  necessary to do aggregation tasks. For example, if each test target specifies
  the output location of its binary to run in a metadata field, that can be
  collected into a single file listing the locations of all tests in the
  dependency tree. A local build tool (or continuous integration infrastructure)
  can then use that file to know which tests exist, and where, and run them
  accordingly.

  Another use is in image creation, where a post-build image tool needs to know
  various pieces of information about the components it should include in order
  to put together the correct image.
  
  元数据可用于收集有关构建中不同目标的信息，因此一个常见的用途是为构建后工具提供执行聚合任务所需的一组数据。例如，如果每个测试目标指定要在元数据字段中运行的二进制文件的输出位置，则可以将其收集到一个文件中，该文件列出了依赖关系树中所有测试的位置。然后，本地构建工具（或持续集成基础设施）可以使用该文件来了解存在哪些测试以及测试的位置，并相应地运行这些测试。              
  另一个用途是在图像创建中，即构建后的图像工具需要了解有关它应该包括的组件的各种信息，以便将正确的图像组合在一起。
```
