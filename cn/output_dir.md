### gn help output_dir
#### **output_dir**: [directory] Directory to put output file in.

```
  For library and executable targets, overrides the directory for the final
  output. This must be in the root_build_dir or a child thereof.

  This should generally be in the root_out_dir or a subdirectory thereof (the
  root_out_dir will be the same as the root_build_dir for the default
  toolchain, and will be a subdirectory for other toolchains). Not putting the
  output in a subdirectory of root_out_dir can result in collisions between
  different toolchains, so you will need to take steps to ensure that your
  target is only present in one toolchain.

  Normally the toolchain specifies the output directory for libraries and
  executables (see "gn help tool"). You will have to consult that for the
  default location. The default location will be used if output_dir is
  undefined or empty.
```

#### **Example**

```
  shared_library("doom_melon") {
    output_dir = "$root_out_dir/plugin_libs"
    ...
  }
```
