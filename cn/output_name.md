### gn help output_name
#### **output_name**: Define a name for the output file other than the default.

```
  Normally the output name of a target will be based on the target name, so the
  target "//foo/bar:bar_unittests" will generate an output file such as
  "bar_unittests.exe" (using Windows as an example).

  Sometimes you will want an alternate name to avoid collisions or if the
  internal name isn't appropriate for public distribution.

  The output name should have no extension or prefixes, these will be added
  using the default system rules. For example, on Linux an output name of "foo"
  will produce a shared library "libfoo.so". There is no way to override the
  output prefix of a linker tool on a per- target basis. If you need more
  flexibility, create a copy target to produce the file you want.

  This variable is valid for all binary output target types.
```

#### **Example**

```
  static_library("doom_melon") {
    output_name = "fluffy_bunny"
  }
```
