### gn help path
#### **gn path &lt;out_dir&gt; &lt;target_one&gt; &lt;target_two&gt;**

```
  在两个targets中查找依赖的路径。每个唯一的路径将会在一个group中被打印出来，每个group直接用换行符分开。
  两个目标将会按照任意顺序显示。
  
  默认情况下将会打印单个路径。如果只有公共依赖的路径，则打印最短公共路径，否则将打印公共或者私有依赖的最短路径。
  如果指定--with-data，则还需要考虑data依赖。如果有多个最短路径，则将选择任意一条路径。
 
  Note: 英文翻译，完全没理解gn path的实际应用场景
```

#### **Interesting paths**

```
  在一个大型项目中，在非常高的等级和普通的低级别目标之间可能有100多个百万条独特的路径。
  为了使得输出更有用，GN不会重新访问已知的子路径去导出目标。
  
  Note：完全没理解
```

#### **Options**

```
  --all
  	 打印所有的"interesting" paths，而不仅仅是第一个路径。
  	 Public paths会按照长度顺序首先打印出来，然后再按照长度顺序打印出non-public paths。

  --public
     只考虑public paths，不能和--with-data同时使用

  --with-data
     增加data依赖关注。如果没有该选项，则只关注public和private依赖，不能和--public同时使用。
```

#### **Example**

```
  gn path out/Default //base //gn
```
