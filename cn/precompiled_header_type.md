### gn help precompiled_header_type
#### **precompiled_header_type**: [string] "gcc" or "msvc".

```
  See "gn help precompiled_header".
```
