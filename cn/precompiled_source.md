### gn help precompiled_source
#### **precompiled_source**: [file name] Source file to precompile.

```
  The source file that goes along with the precompiled_header when using
  "msvc"-style precompiled headers. It will be implicitly added to the sources
  of the target. See "gn help precompiled_header".
```
