### gn help read_file
#### **read_file**: 将文件内容读入变量。

```
  read_file(filename, input_conversion)

  Whitespace will be trimmed from the end of the file. Throws an error if the
  file can not be opened.
  将文件内容读入变量中，文件末尾的空格会被去除，如果文件无法打开则报错。
```

#### **Arguments**

参考说明：[io_conversion](./all.md/#io_conversion)

```
  filename
      要读取的文件，文件路径是相对于构建文件的路径。

  input_conversion
      控制文件读取和解析方式，详情参见"gn help io_conversion"。
```

#### **Example**

```
  lines = read_file("foo.txt", "list lines")
```
