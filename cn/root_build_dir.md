### gn help root_build_dir
#### **root_build_dir**: [string] Directory where build commands are run.

```
  This is the root build output directory which will be the current directory
  when executing all compilers and scripts.

  Most often this is used with rebase_path (see "gn help rebase_path") to
  convert arguments to be relative to a script's current directory.
```
