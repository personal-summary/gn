### gn help root_out_dir
#### **root_out_dir**: [string] Root directory for toolchain output files.

```
  Absolute path to the root of the output directory tree for the current
  toolchain. It will not have a trailing slash.

  For the default toolchain this will be the same as the root_build_dir. An
  example would be "//out/Debug" for the default toolchain, or
  "//out/Debug/arm" for the "arm" toolchain.

  This is primarily useful for setting up script calls. If you are passing this
  to a script, you will want to pass it through rebase_path() (see "gn help
  rebase_path") to convert it to be relative to the build directory.

  See also "target_out_dir" which is usually a better location for output
  files. It will be inside the root output dir.
```

#### **Example**

```
  action("myscript") {
    # Pass the output dir to the script.
    args = [ "-o", rebase_path(root_out_dir, root_build_dir) ]
  }
```
