### gn help source_expansion
#### **How Source Expansion Works**

参考：[gn help copy](./copy.md)和[gn help action_foreach](./action_foreach.md)

```
  Source expansion主要是用于action_foreach和copy操作中，用来将源文件名字映射到输出文件名或者参数中。
  
  为了在输出中执行源扩展，GN会将sources中所有条目一一对应的映射到输出列表的每个条目中，生成所有组合的交叉集合，也就是扩展占位符。
  
  args中的源扩展原理也是类似，但是执行占位符替换会为每次脚本调用生成一组不同的参数。
  
  如果没有找到占位符，则会将输出或者参数列表当作不依赖sources的静态列表。
  
  参考"gn help copy"和"gn help action_foreach"了解更多的应用场景。
```

#### **Placeholders**

参考：[gn help tool](./tool.md)

```
  本章节只讨论占位符的使用场景。
  tools的定义中还使用了其他占位符，想了解的请参考"gn help tool"。

  {{source}}
      源文件同时包含目录的路径。这一般用于脚本变量的指定输入中。
      例如："//foo/bar/baz.txt" => "../../foo/bar/baz.txt"

  {{source_file_part}}
      源文件名称不包括所在目录的路径，仅仅是源文件名称。
      例如："//foo/bar/baz.txt" => "baz.txt"

  {{source_name_part}}
      只是源文件的文件名部分，不包括后缀和目录路径。
      这主要是用于将源文件进行后缀名替换的操作使用当中。
      例如："//foo/bar/baz.txt" => "baz"

  {{source_dir}}
      获取源文件的路径名称。
      例如："//foo/bar/baz.txt" => "../../foo/bar"

  {{source_root_relative_dir}}
      源文件目录相对于根目录的路径，这个路径没有//和尾部的/。
      如果路径是系统绝对路径（以/开头），则只会返回没有尾部/的路径，不管它出现在outputs还是args部分，值都是一样的。
      例如："//foo/bar/baz.txt" => "foo/bar"

  {{source_gen_dir}}
      生成文件的路径相对于源文件的路径。
      如果源文件与 BUILD.gn 文件位于不同的目录中，这将与目标生成的文件目录不同。
      例如："//foo/bar/baz.txt" => "gen/foo/bar"

  {{source_out_dir}}
      中间对象文件的路径相对于源文件的路径。
      如果源文件与 build.gn 文件位于不同的目录中，则该路径与目标out路径不同。
      例如："//foo/bar/baz.txt" => "obj/foo/bar"

  {{source_target_relative}}
      源文件相对于目标目录的路径。
      这通常用于复制输出目录中的源目录布局，这只能在操作和bundle_data目标中使用。
      不能直接在没有target的process_file_template中使用，否则会报错。
      例如："//foo/bar/baz.txt" => "baz.txt"
```

#### **(*) Note on directories**

```
  包含目录的路径（除了source_root_relative_dir） 将根据评估扩展的上下文而有所不同。
  通常，它应该“正常工作”，但这意味着您无法将包含这些值的字符串与合理的结果连接起来。
  
  源扩展可以在outputs的变量、args变量以及process_file_template中调用使用。
  “args”会被传递给构建目录的运行脚本中，因此这些目录将相对于构建目录供脚本查找。
  在其他情况下，路径是源文件的绝对路径，以//开头的，因为这些扩展的结果会在GN的内部处理。
```

#### **Examples**

```
  Non-varying outputs:
    action("hardcoded_outputs") {
      sources = [ "input1.idl", "input2.idl" ]
      outputs = [ "$target_out_dir/output1.dat",
                  "$target_out_dir/output2.dat" ]
    }
  The outputs in this case will be the two literal files given.

  Varying outputs:
    action_foreach("varying_outputs") {
      sources = [ "input1.idl", "input2.idl" ]
      outputs = [ "{{source_gen_dir}}/{{source_name_part}}.h",
                  "{{source_gen_dir}}/{{source_name_part}}.cc" ]
    }
  Performing source expansion will result in the following output names:
    //out/Debug/obj/mydirectory/input1.h
    //out/Debug/obj/mydirectory/input1.cc
    //out/Debug/obj/mydirectory/input2.h
    //out/Debug/obj/mydirectory/input2.cc
```
