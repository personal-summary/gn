### gn help static_library
#### **static_library**: Declare a static library target.

```
  Make a ".a" / ".lib" file.

  If you only need the static library for intermediate results in the build,
  you should consider a source_set instead since it will skip the (potentially
  slow) step of creating the intermediate library file.
  
  如果您只需要静态库来生成中间结果，则应考虑使用source_set，因为它将跳过创建中间库文件的步骤（可能会很慢）。
```

#### **Variables**

```
  complete_static_lib
  Flags: cflags, cflags_c, cflags_cc, cflags_objc, cflags_objcc,
         asmflags, defines, include_dirs, inputs, ldflags, lib_dirs,
         libs, precompiled_header, precompiled_source, rustflags,
         rustenv
  Deps: data_deps, deps, public_deps
  Dependent configs: all_dependent_configs, public_configs
  General: check_includes, configs, data, friend, inputs, metadata,
           output_name, output_extension, public, sources, testonly,
           visibility
  Rust variables: aliased_deps, crate_root, crate_name

  The tools and commands used to create this target type will be
  determined by the source files in its sources. Targets containing
  multiple compiler-incompatible languages are not allowed (e.g. a
  target containing both C and C++ sources is acceptable, but a
  target containing C and Rust sources is not).
  用于创建此目标类型的工具和命令将由其源中的源文件确定。不允许包含多个编译器不兼容语言的目标（例如，包含C和C++源的目标是可接受的，但包含C和Rust源的目标不是）。
```
