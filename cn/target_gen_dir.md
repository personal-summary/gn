### gn help target_gen_dir
#### **target_gen_dir**: Directory for a target's generated files.

```
  Absolute path to the target's generated file directory. This will be the
  "root_gen_dir" followed by the relative path to the current build file. If
  your file is in "//tools/doom_melon" then target_gen_dir would be
  "//out/Debug/gen/tools/doom_melon". It will not have a trailing slash.

  This is primarily useful for setting up include paths for generated files. If
  you are passing this to a script, you will want to pass it through
  rebase_path() (see "gn help rebase_path") to convert it to be relative to the
  build directory.

  See also "gn help root_gen_dir".
```

#### **Example**

```
  action("myscript") {
    # Pass the generated output dir to the script.
    args = [ "-o", rebase_path(target_gen_dir, root_build_dir) ]"
  }
```
