### gn help target_name
#### **target_name**: [string] The name of the current target.

```
  Inside a target or template invocation, this variable refers to the name
  given to the target or template invocation. Outside of these, this variable
  is undefined.

  This is most often used in template definitions to name targets defined in
  the template based on the name of the invocation. This is necessary both to
  ensure generated targets have unique names and to generate a target with the
  exact name of the invocation that other targets can depend on.

  Be aware that this value will always reflect the innermost scope. So when
  defining a target inside a template, target_name will refer to the target
  rather than the template invocation. To get the name of the template
  invocation in this case, you should save target_name to a temporary variable
  outside of any target definitions.

  See "gn help template" for more examples.
```

#### **Example**

```
  executable("doom_melon") {
    print(target_name)    # Prints "doom_melon".
  }

  template("my_template") {
    print(target_name)    # Prints "space_ray" when invoked below.

    executable(target_name + "_impl") {
      print(target_name)  # Prints "space_ray_impl".
    }
  }

  my_template("space_ray") {
  }
```
