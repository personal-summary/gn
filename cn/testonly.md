### gn help testonly
#### **testonly**: Declares a target must only be used for testing.

```
  Boolean. Defaults to false.

  When a target is marked "testonly = true", it must only be depended on by
  other test-only targets. Otherwise, GN will issue an error that the
  depenedency is not allowed.

  This feature is intended to prevent accidentally shipping test code in a
  final product.
```

#### **Example**

```
  source_set("test_support") {
    testonly = true
    ...
  }
```
