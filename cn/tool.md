### gn help tool
#### **tool**: Specify arguments to a toolchain tool.

#### **Usage**

```
  tool(<tool type>) {
    <tool variables...>
  }
```

#### **Tool types**

```
    Compiler tools: 编译器
      "cc": C compiler
      "cxx": C++ compiler
      "objc": Objective C compiler
      "objcxx": Objective C++ compiler
      "rc": Resource compiler (Windows .rc files)
      "asm": Assembler

    Linker tools:链接器
      "alink": Linker for static libraries (archives)
      "solink": Linker for shared libraries
      "link": Linker for executables

    Other tools:其他
      "stamp": Tool for creating stamp files
      "copy": Tool to copy files.
      "action": Defaults for actions

    Platform specific tools:平台专用工具
      "copy_bundle_data": [iOS, macOS] Tool to copy files in a bundle.
      "compile_xcassets": [iOS, macOS] Tool to compile asset catalogs.

    Rust tools:
      "rust_bin": Tool for compiling Rust binaries
      "rust_cdylib": Tool for compiling C-compatible dynamic libraries.
      "rust_dylib": Tool for compiling Rust dynamic libraries.
      "rust_macro": Tool for compiling Rust procedural macros.
      "rust_rlib": Tool for compiling Rust libraries.
      "rust_staticlib": Tool for compiling Rust static libraries.
```

#### **Tool variables**

```
    command  [string with substitutions]
        Valid for: all tools except "action" (required)

        The command to run.
        适用于：除“action”（必需）外的所有工具              
        要运行的命令。

    command_launcher  [string]
        Valid for: all tools except "action" (optional)

        The prefix with which to launch the command (e.g. the path to a Goma or
        CCache compiler launcher).

        Note that this prefix will not be included in the compilation database or
        IDE files generated from the build.
        
        适用于：除“action”（可选）以外的所有工具              
        用于启动命令的前缀（例如Goma或CCache编译器启动器的路径）。              
        请注意，此前缀不会包含在编译数据库或生成的IDE文件中。

    default_output_dir  [string with substitutions]
        Valid for: linker tools

        Default directory name for the output file relative to the
        root_build_dir. It can contain other substitution patterns. This will
        be the default value for the {{output_dir}} expansion (discussed below)
        but will be overridden by the "output_dir" variable in a target, if one
        is specified.

        GN doesn't do anything with this string other than pass it along,
        potentially with target-specific overrides. It is the tool's job to use
        the expansion so that the files will be in the right place.
        
        适用于：链接器工具              
        输出文件相对于root_build_dir的默认目录名。它可以包含其他替换模式。这将是｛｛output_dir｝｝扩展的默认值（如下所述），但如果指定了一个变量，则会被         目标中的“output_dr”变量覆盖。              
        GN对该字符串不做任何操作，只是传递它，可能会使用特定于目标的重写。工具的工作是使用扩展，以便文件位于正确的位置。

    default_output_extension  [string]
        Valid for: linker tools

        Extension for the main output of a linkable tool. It includes the
        leading dot. This will be the default value for the
        {{output_extension}} expansion (discussed below) but will be overridden
        by by the "output extension" variable in a target, if one is specified.
        Empty string means no extension.

        GN doesn't actually do anything with this extension other than pass it
        along, potentially with target-specific overrides. One would typically
        use the {{output_extension}} value in the "outputs" to read this value.

        Example: default_output_extension = ".exe"
        
        适用于：链接器工具              
        可链接工具主输出的扩展。它包括前导点。这将是｛｛output_extension｝｝扩展（下面讨论）的默认值，但如果指定了一个变量，则会被目标中的“输出扩展”变量         覆盖。空字符串表示没有扩展名。              
        GN实际上对这个扩展不做任何事情，只是传递它，可能会使用特定于目标的重写。通常会使用“输出”中的｛｛output_extension｝｝值来读取该值。                    示例：default_output_extension=“.exe”

    depfile  [string with substitutions]
        Valid for: compiler tools (optional)

        If the tool can write ".d" files, this specifies the name of the
        resulting file. These files are used to list header file dependencies
        (or other implicit input dependencies) that are discovered at build
        time. See also "depsformat".

        Example: depfile = "{{output}}.d"
        
        适用于：编译器工具（可选）              
        如果工具可以写入“.d”文件，则指定生成文件的名称。这些文件用于列出在构建时发现的头文件依赖项（或其他隐式输入依赖项）。另请参见“depsfomat”。              示例：depfile=“｛｛output｝｝.d”

    depsformat  [string]
        Valid for: compiler tools (when depfile is specified)

        Format for the deps outputs. This is either "gcc" or "msvc". See the
        ninja documentation for "deps" for more information.

        Example: depsformat = "gcc"
        
        适用于：编译器工具（指定depfile时）              
        deps输出的格式。这是“gcc”或“msvc”。有关更多信息，请参阅忍者文档中的“deps”。              
        示例：depsfomat=“gcc”

    description  [string with substitutions, optional]
        Valid for: all tools

        What to print when the command is run.

        Example: description = "Compiling {{source}}"
        适用于：所有工具              
        运行命令时要打印的内容。              
        示例：description=“正在编译｛｛source｝｝”

    exe_output_extension [string, optional, rust tools only]
    rlib_output_extension [string, optional, rust tools only]
    dylib_output_extension [string, optional, rust tools only]
    cdylib_output_extension [string, optional, rust tools only]
    rust_proc_macro_output_extension [string, optional, rust tools only]
        Valid for: Rust tools

        These specify the default tool output for each of the crate types.
        The default is empty for executables, shared, and static libraries and
        ".rlib" for rlibs. Note that the Rust compiler complains with an error
        if external crates do not take the form `lib<name>.rlib` or
        `lib<name>.<shared_extension>`, where `<shared_extension>` is `.so`,
        `.dylib`, or `.dll` as appropriate for the platform.
        适用于：Rust工具              
        它们为每个板条箱类型指定默认工具输出。可执行文件、共享库和静态库的默认值为空，rlib的默认值是“.rib”。请注意，如果外部板条箱不采用`lib<name>格式，         Rust编译器会发出错误。rlib`或`lib<name><shared_extension>`，其中`<shared_extension>`是`。所以“，”。dylib或。dll”，以适合平台。

    lib_switch  [string, optional, link tools only]
    lib_dir_switch  [string, optional, link tools only]
        Valid for: Linker tools except "alink"

        These strings will be prepended to the libraries and library search
        directories, respectively, because linkers differ on how to specify
        them.

        If you specified:
          lib_switch = "-l"
          lib_dir_switch = "-L"
        then the "{{libs}}" expansion for
          [ "freetype", "expat" ]
        would be
          "-lfreetype -lexpat".
          适用于：链接器工具，“alink”除外              
          这些字符串将分别添加到库和库搜索目录中，因为链接器在如何指定它们方面有所不同。              
          如果您指定：              
          lib_switch=“-l”              
          lib_dir_switch=“-L”              
          然后是的“｛｛libs｝｝”扩展              
          [“freetype”，“expat”]              
          将是              
          “-lfreetype-lexpat”。

    framework_switch [string, optional, link tools only]
    framework_dir_switch [string, optional, link tools only]
        Valid for: Linker tools

        These strings will be prepended to the frameworks and framework search
        path directories, respectively, because linkers differ on how to specify
        them.

        If you specified:
          framework_switch = "-framework "
          framework_dir_switch = "-F"
        then the "{{libs}}" expansion for
          [ "UIKit.framework", "$root_out_dir/Foo.framework" ]
        would be
          "-framework UIKit -F. -framework Foo"
          适用于：链接器工具              
          这些字符串将分别添加到框架和框架搜索路径目录中，因为链接器在如何指定它们方面有所不同。              
          如果您指定：              
          framework_switch=“-framework”              
          framework_dir_switch=“-F”              
          然后是的“｛｛libs｝｝”扩展              
          [“UIKit.framework”，“$root_out_dir/Foo.framework”]              
          将是              
          “-framework UIKit-F-framework Foo”

    outputs  [list of strings with substitutions]
        Valid for: Linker and compiler tools (required)

        An array of names for the output files the tool produces. These are
        relative to the build output directory. There must always be at least
        one output file. There can be more than one output (a linker might
        produce a library and an import library, for example).

        This array just declares to GN what files the tool will produce. It is
        your responsibility to specify the tool command that actually produces
        these files.

        If you specify more than one output for shared library links, you
        should consider setting link_output, depend_output, and
        runtime_outputs.

        Example for a compiler tool that produces .obj files:
          outputs = [
            "{{source_out_dir}}/{{source_name_part}}.obj"
          ]

        Example for a linker tool that produces a .dll and a .lib. The use of
        {{target_output_name}}, {{output_extension}} and {{output_dir}} allows
        the target to override these values.
          outputs = [
            "{{output_dir}}/{{target_output_name}}"
                "{{output_extension}}",
            "{{output_dir}}/{{target_output_name}}.lib",
          ]
          
          适用于：链接器和编译器工具（必需）              
          工具生成的输出文件的名称数组。这些是相对于生成输出目录的。必须始终至少有一个输出文件。可以有多个输出（例如，链接器可能会生成一个库和一个导入库）。           此数组仅向GN声明工具将生成的文件。您有责任指定实际生成这些文件的工具命令。              
          如果为共享库链接指定了多个输出，则应考虑设置link_output、dependent_output和runtime_output。              
          生成.obj文件的编译器工具示例：              
          输出=[              
          “｛｛source_out_dir｝｝/｛{source_name_part｝｝.obj”              
          ]              
          生成.dll和.lib的链接器工具示例。使用｛｛target_output_name｝｝、｛{output_extension｝和｛{output_dir｝允许目标覆盖这些值。                   输出=[              
          “｛｛output_dir｝｝/｛{target_output_name｝”              
          “｛｛output_extension｝｝”，              
          “｛{output_dir｝｝/｛{target_output_name｝｝.lib”，              
          ]

    pool [label, optional]
        Valid for: all tools (optional)

        Label of the pool to use for the tool. Pools are used to limit the
        number of tasks that can execute concurrently during the build.

        See also "gn help pool".
        
        适用于：所有工具（可选）              
        用于工具的池的标签。池用于限制在生成期间可以并发执行的任务数。              
        另请参阅“gn帮助池”。

    link_output  [string with substitutions]
    depend_output  [string with substitutions]
        Valid for: "solink" only (optional)

        These two files specify which of the outputs from the solink tool
        should be used for linking and dependency tracking. These should match
        entries in the "outputs". If unspecified, the first item in the
        "outputs" array will be used for all. See "Separate linking and
        dependencies for shared libraries" below for more.

        On Windows, where the tools produce a .dll shared library and a .lib
        import library, you will want the first two to be the import library
        and the third one to be the .dll file. On Linux, if you're not doing
        the separate linking/dependency optimization, all of these should be
        the .so output.
        仅适用于：“solink”（可选）              
        这两个文件指定了solink工具的哪些输出应用于链接和相关性跟踪。这些应与“输出”中的条目相匹配。如果未指定，“outputs”数组中的第一项将用于所有项。有关详细信息，请参阅下面的“共享库的单独链接和依赖关系”。              
        在Windows上，工具生成一个.dll共享库和一个.lib导入库，您希望前两个是导入库，第三个是.dll文件。在Linux上，如果不进行单独的链接/依赖优化，所有这些都应该是.so输出。

    output_prefix  [string]
        Valid for: Linker tools (optional)

        Prefix to use for the output name. Defaults to empty. This prefix will
        be prepended to the name of the target (or the output_name if one is
        manually specified for it) if the prefix is not already there. The
        result will show up in the {{output_name}} substitution pattern.

        Individual targets can opt-out of the output prefix by setting:
          output_prefix_override = true
        (see "gn help output_prefix_override").

        This is typically used to prepend "lib" to libraries on
        Posix systems:
          output_prefix = "lib"
          
          适用于：链接器工具（可选）              
          用于输出名称的前缀。默认为空。如果前缀不存在，则该前缀将附加到目标的名称（或output_name，如果手动为其指定）。结果将显示在｛｛output_name｝｝替换模式中。              
          单个目标可以通过以下设置退出输出前缀：              
          output_prix_override=真              
          （请参阅“gn help output_prefix_override”）。              
          这通常用于在              
          Posix系统：              
          outputprefix=“lib”

    precompiled_header_type  [string]
        Valid for: "cc", "cxx", "objc", "objcxx"

        Type of precompiled headers. If undefined or the empty string,
        precompiled headers will not be used for this tool. Otherwise use "gcc"
        or "msvc".

        For precompiled headers to be used for a given target, the target (or a
        config applied to it) must also specify a "precompiled_header" and, for
        "msvc"-style headers, a "precompiled_source" value. If the type is
        "gcc", then both "precompiled_header" and "precompiled_source" must
        resolve to the same file, despite the different formats required for
        each."

        See "gn help precompiled_header" for more.
        
        适用于：“cc”、“cxx”、“objc”、“objcxx”              
        预编译头的类型。如果未定义或为空字符串，预编译头将不会用于此工具。否则使用“gcc”或“msvc”。              
        对于要用于给定目标的预编译头，目标（或应用于它的配置）还必须指定“precompiled_header”，对于“msvc”样式的头，还必须指定一个“precompile d_source”值。如果类型为“gcc”，则“precompiled_header”和“precompile d_source”必须解析为同一个文件，尽管每个文件所需的格式不同              
        有关详细信息，请参阅“gn help precompiled_header”。

    restat  [boolean]
        Valid for: all tools (optional, defaults to false)

        Requests that Ninja check the file timestamp after this tool has run to
        determine if anything changed. Set this if your tool has the ability to
        skip writing output if the output file has not changed.

        Normally, Ninja will assume that when a tool runs the output be new and
        downstream dependents must be rebuild. When this is set to trye, Ninja
        can skip rebuilding downstream dependents for input changes that don't
        actually affect the output.

        Example:
          restat = true
          
          适用于：所有工具（可选，默认为false）              
          请求Ninja在运行此工具后检查文件时间戳，以确定是否有任何更改。如果您的工具能够在输出文件未更改的情况下跳过写入输出，请设置此选项。              通常，Ninja会假设当一个工具运行时，输出是新的，下游从属项必须重建。当设置为trye时，Ninja可以跳过重建下游从属项，以进行实际上不会影响输出的输入更改。              例子：              
          restat=真

    rspfile  [string with substitutions]
        Valid for: all tools except "action" (optional)

        Name of the response file. If empty, no response file will be
        used. See "rspfile_content".
        适用于：除“操作”（可选）以外的所有工具             
        响应文件的名称。如果为空，则不使用响应文件。请参见“rspfile_content”。

    rspfile_content  [string with substitutions]
        Valid for: all tools except "action" (required when "rspfile" is used)

        The contents to be written to the response file. This may include all
        or part of the command to send to the tool which allows you to get
        around OS command-line length limits.

        This example adds the inputs and libraries to a response file, but
        passes the linker flags directly on the command line:
          tool("link") {
            command = "link -o {{output}} {{ldflags}} @{{output}}.rsp"
            rspfile = "{{output}}.rsp"
            rspfile_content = "{{inputs}} {{solibs}} {{libs}}"
          }
          适用于：除“action”以外的所有工具（使用“rspfile”时需要）              
          要写入响应文件的内容。这可能包括要发送到工具的全部或部分命令，该工具允许您绕过OS命令行长度限制。              
          此示例将输入和库添加到响应文件中，但直接在命令行上传递链接器标志：
          tool("link") {
            command = "link -o {{output}} {{ldflags}} @{{output}}.rsp"
            rspfile = "{{output}}.rsp"
            rspfile_content = "{{inputs}} {{solibs}} {{libs}}"
          }

    runtime_outputs  [string list with substitutions]
        Valid for: linker tools

        If specified, this list is the subset of the outputs that should be
        added to runtime deps (see "gn help runtime_deps"). By default (if
        runtime_outputs is empty or unspecified), it will be the link_output.
        适用于：链接器工具              
        如果指定，此列表是应添加到运行时deps的输出的子集（请参阅“gn-helpruntime_deps”）。默认情况下（如果runtime_output为空或未指定），它将是link_output。
```

#### **Expansions for tool variables**

```
  All paths are relative to the root build directory, which is the current
  directory for running all tools. These expansions are available to all tools:

    {{label}}
        The label of the current target. This is typically used in the
        "description" field for link tools. The toolchain will be omitted from
        the label for targets in the default toolchain, and will be included
        for targets in other toolchains.

    {{label_name}}
        The short name of the label of the target. This is the part after the
        colon. For "//foo/bar:baz" this will be "baz". Unlike
        {{target_output_name}}, this is not affected by the "output_prefix" in
        the tool or the "output_name" set on the target.

    {{output}}
        The relative path and name of the output(s) of the current build step.
        If there is more than one output, this will expand to a list of all of
        them. Example: "out/base/my_file.o"

    {{target_gen_dir}}
    {{target_out_dir}}
        The directory of the generated file and output directories,
        respectively, for the current target. There is no trailing slash. See
        also {{output_dir}} for linker tools. Example: "out/base/test"

    {{target_output_name}}
        The short name of the current target with no path information, or the
        value of the "output_name" variable if one is specified in the target.
        This will include the "output_prefix" if any. See also {{label_name}}.

        Example: "libfoo" for the target named "foo" and an output prefix for
        the linker tool of "lib".
     所有路径都相对于根构建目录，该目录是运行所有工具的当前目录。这些扩展适用于所有工具：              
     ｛｛标签｝｝              
     当前目标的标签。这通常用于链接工具的“描述”字段中。默认工具链中目标的标签中将省略该工具链，其他工具链中的目标将包含该工具链。                       ｛｛label_name｝｝              
     目标标签的短名称。这是冒号后面的部分。对于“//foo/bar:baz”，这将是“baz”。与｛｛target_output_name｝｝不同，这不受工具中的“outputprefix”或目标上设置的“outut_name”的影响。              
     ｛｛输出｝｝              
     当前生成步骤的输出的相对路径和名称。如果有多个输出，则将扩展为所有输出的列表。示例：“out/base/my_file.o”              
     ｛｛target_gen_dir｝｝              
     ｛｛target_out_dir｝｝              
     分别为当前目标生成的文件和输出目录的目录。没有尾随斜杠。有关链接器工具，请参见｛｛output_dir｝｝。示例：“out/base/test”              ｛｛target_output_name｝｝              
     没有路径信息的当前目标的短名称，或“output_name”变量的值（如果在目标中指定了一个）。这将包括“outputprefix”（如果有）。另请参｛｛label_name｝｝。              
     示例：名为“foo”的目标为“libfoo”，链接器工具为“lib”的输出前缀。   

  Compiler tools have the notion of a single input and a single output, along
  with a set of compiler-specific flags. The following expansions are
  available:

    {{asmflags}}
    {{cflags}}
    {{cflags_c}}
    {{cflags_cc}}
    {{cflags_objc}}
    {{cflags_objcc}}
    {{defines}}
    {{include_dirs}}
        Strings correspond that to the processed flags/defines/include
        directories specified for the target.
        Example: "--enable-foo --enable-bar"

        Defines will be prefixed by "-D" and include directories will be
        prefixed by "-I" (these work with Posix tools as well as Microsoft
        ones).

    {{source}}
        The relative path and name of the current input file.
        Example: "../../base/my_file.cc"

    {{source_file_part}}
        The file part of the source including the extension (with no directory
        information).
        Example: "foo.cc"

    {{source_name_part}}
        The filename part of the source file with no directory or extension.
        Example: "foo"

    {{source_gen_dir}}
    {{source_out_dir}}
        The directory in the generated file and output directories,
        respectively, for the current input file. If the source file is in the
        same directory as the target is declared in, they will will be the same
        as the "target" versions above. Example: "gen/base/test"
        编译器工具具有单个输入和单个输出的概念，以及一组特定于编译器的标志。以下扩展可用：              
        ｛｛asmflags｝｝              
        ｛｛cflags｝｝              
        ｛｛cflags_c｝｝              
        ｛｛cflags_cc｝｝              
        ｛｛cflags_objc｝｝              
        ｛｛cflags_objcc｝｝              
        ｛｛定义｝｝              
        ｛｛include_dirs｝｝              
        字符串对应于为目标指定的已处理标志/定义/包含目录。              
        示例：“--enable foo--enable bar”              
        定义将以“-D”为前缀，包含目录将以“-I”为前缀（这些与Posix工具以及Microsoft工具一起使用）。              
        ｛｛源｝｝              
        当前输入文件的相对路径和名称。示例：“../../base/my_file.cc”              
        ｛｛source_file_part｝｝              
        源的文件部分，包括扩展名（没有目录信息）。              
        示例：“foo.cc”              
        ｛｛source_name_part｝｝              
        源文件的文件名部分，没有目录或扩展名。示例：“foo”              
        ｛｛source_gen_dir｝｝              
        ｛｛source_out_dir｝｝              
        当前输入文件的生成文件和输出目录中的目录。如果源文件与中声明的目标文件位于同一目录中，则它们将与上面的“目标”版本相同。示例：“gen/base/test”

  Linker tools have multiple inputs and (potentially) multiple outputs. The
  static library tool ("alink") is not considered a linker tool. The following
  expansions are available:

    {{inputs}}
    {{inputs_newline}}
        Expands to the inputs to the link step. This will be a list of object
        files and static libraries.
        Example: "obj/foo.o obj/bar.o obj/somelibrary.a"

        The "_newline" version will separate the input files with newlines
        instead of spaces. This is useful in response files: some linkers can
        take a "-filelist" flag which expects newline separated files, and some
        Microsoft tools have a fixed-sized buffer for parsing each line of a
        response file.

    {{ldflags}}
        Expands to the processed set of ldflags and library search paths
        specified for the target.
        Example: "-m64 -fPIC -pthread -L/usr/local/mylib"

    {{libs}}
        Expands to the list of system libraries to link to. Each will be
        prefixed by the "lib_switch".

        As a special case to support Mac, libraries with names ending in
        ".framework" will be added to the {{libs}} with "-framework" preceding
        it, and the lib prefix will be ignored.

        Example: "-lfoo -lbar"

    {{output_dir}}
        The value of the "output_dir" variable in the target, or the the value
        of the "default_output_dir" value in the tool if the target does not
        override the output directory. This will be relative to the
        root_build_dir and will not end in a slash. Will be "." for output to
        the root_build_dir.

        This is subtly different than {{target_out_dir}} which is defined by GN
        based on the target's path and not overridable. {{output_dir}} is for
        the final output, {{target_out_dir}} is generally for object files and
        other outputs.

        Usually {{output_dir}} would be defined in terms of either
        {{target_out_dir}} or {{root_out_dir}}

    {{output_extension}}
        The value of the "output_extension" variable in the target, or the
        value of the "default_output_extension" value in the tool if the target
        does not specify an output extension.
        Example: ".so"

    {{solibs}}
        Extra libraries from shared library dependencies not specified in the
        {{inputs}}. This is the list of link_output files from shared libraries
        (if the solink tool specifies a "link_output" variable separate from
        the "depend_output").

        These should generally be treated the same as libs by your tool.

        Example: "libfoo.so libbar.so"

    {{frameworks}}
        Shared libraries packaged as framework bundle. This is principally
        used on Apple's platforms (macOS and iOS). All name must be ending
        with ".framework" suffix; the suffix will be stripped when expanding
        {{frameworks}} and each item will be preceded by "-framework".
        链接器工具具有多个输入和（可能）多个输出。静态库工具（“alink”）不被视为链接器工具。以下扩展可用：              
        ｛｛输入｝｝              
        ｛｛inputs_newline｝｝              
        扩展到链接步骤的输入。这将是一个对象文件和静态库的列表。              
        示例：“obj/foo.oobj/bar.oobj/somelibrary.a”              
        “_newline”版本将用换行符而不是空格分隔输入文件。这在响应文件中很有用：一些链接器可以使用“-filelist”标志，该标志需要换行的文件，而一些Microsoft工具有固定大小的缓冲区用于解析响应文件的每一行。              
        ｛｛ldflags｝｝              
        扩展到为目标指定的已处理的ldflags和库搜索路径集。              
        示例：“-m64-fPIC-phread-L/usr/local/mylib”              
        ｛｛libs｝｝              
        扩展到要链接到的系统库列表。每个库将以“lib_switch”为前缀。              
        作为支持Mac的一种特殊情况，名称以“.framework”结尾的库将添加到前面带有“-framework”的｛｛libs｝｝中，并忽略lib前缀。              
        示例：“-lfo-lbar”              
        ｛｛output_dir｝｝              
        目标中“output_dir”变量的值，如果目标未覆盖输出目录，则为工具中“default_output_dir”值的值。这是相对于root_build_dir的，不会以斜杠结尾。对于root_build_dir的输出，将为“.”。              
        这与｛｛target_out_dir｝｝略有不同，后者由GN基于目标的路径定义，不可重写。｛{output_dir｝｝用于最终输出，｛{target_out_dir｝｝一般用于对象文件和其他输出。              
        通常｛｛output_dir｝｝将根据              
        ｛｛target_out_dir｝｝或｛{root_out_dir}｝              
        ｛｛output_extension｝｝              
        目标中“output_extension”变量的值，或者如果目标未指定输出扩展名，则该工具中“default_output_extendision”值的值。              
        示例：“.so”              
        ｛｛solibs｝｝              
        ｛｛inputs｝｝中未指定来自共享库依赖项的额外库。这是来自共享库的link_output文件列表（如果solink工具指定了与“dependent_output”不同的“link_output”变量）。              
        您的工具通常应将这些与libs相同。              
        示例：“libfoo.so libbar.so”              
        ｛｛框架｝｝              
        共享库打包为框架包。这主要用于苹果的平台（macOS和iOS）。所有名称必须以“.framework”后缀结尾；扩展｛｛framework｝｝时，后缀将被去掉，每个项目前面都将加上“-framework”。

  The static library ("alink") tool allows {{arflags}} plus the common tool
  substitutions.

  The copy tool allows the common compiler/linker substitutions, plus
  {{source}} which is the source of the copy. The stamp tool allows only the
  common tool substitutions.

  The copy_bundle_data and compile_xcassets tools only allows the common tool
  substitutions. Both tools are required to create iOS/macOS bundles and need
  only be defined on those platforms.

  The copy_bundle_data tool will be called with one source and needs to copy
  (optionally optimizing the data representation) to its output. It may be
  called with a directory as input and it needs to be recursively copied.

  The compile_xcassets tool will be called with one or more source (each an
  asset catalog) that needs to be compiled to a single output. The following
  substitutions are available:

    {{inputs}}
        Expands to the list of .xcassets to use as input to compile the asset
        catalog.

    {{bundle_product_type}}
        Expands to the product_type of the bundle that will contain the
        compiled asset catalog. Usually corresponds to the product_type
        property of the corresponding create_bundle target.

    {{bundle_partial_info_plist}}
        Expands to the path to the partial Info.plist generated by the
        assets catalog compiler. Usually based on the target_name of
        the create_bundle target.

  Rust tools have the notion of a single input and a single output, along
  with a set of compiler-specific flags. The following expansions are
  available:

    {{crate_name}}
        Expands to the string representing the crate name of target under
        compilation.

    {{crate_type}}
        Expands to the string representing the type of crate for the target
        under compilation.

    {{externs}}
        Expands to the list of --extern flags needed to include addition Rust
        libraries in this target. Includes any specified renamed dependencies.

    {{rustdeps}}
        Expands to the list of -Ldependency=<path> strings needed to compile
        this target.

    {{rustenv}}
        Expands to the list of environment variables.

    {{rustflags}}
        Expands to the list of strings representing Rust compiler flags.
        静态库（“alink”）工具允许｛｛arflags｝｝加上常用工具替换。              
        复制工具允许常见的编译器/链接器替换，以及作为复制源的｛｛source｝｝。印章工具仅允许替换常用工具。              
        copy_bandle_data和compile_xcasets工具仅允许通用工具替换。这两种工具都是创建iOS/macOS捆绑包所必需的，并且只需要在这些平台上定义。              copy_bindle_data工具将使用一个源调用，需要将其复制（可选地优化数据表示）到其输出。可以使用目录作为输入调用它，并且需要递归复制它。                compile_xcasets工具将使用一个或多个需要编译为单个输出的源（每个源都是一个资产目录）调用。以下替代品可用：              
        ｛｛输入｝｝              
        扩展到.xcapassets列表，用作编译资产目录的输入。              
        ｛｛bundle_product_type｝｝              
        展开到将包含压缩资产目录的捆绑包的product_type。通常对应于相应create_bundle目标的product_type属性。              ｛｛bundle_partial_info_plist｝｝              
        展开到由资产目录编译器生成的部分Info.plist的路径。通常基于create_bundle目标的target_name。              
        Rust工具具有单个输入和单个输出的概念，以及一组编译器特定的标志。以下扩展是              
        可用：              
        ｛｛板条箱名称｝｝              
        展开为表示正在编译的目标的板条箱名称的字符串。              
        ｛｛板条箱类型｝｝              
        展开为表示编译目标的板条箱类型的字符串。              
        ｛｛外部｝｝              
        扩展到需要的--extern标志列表，以便在此目标中添加Rust库。包括任何指定的重命名依赖项。              
        ｛｛rustdeps｝｝              
        展开到编译此目标所需的-Ldependency=<path>字符串列表。              
        ｛｛rustenv｝｝              
        展开到环境变量列表。              
        ｛｛rustflags｝｝              
        展开到表示Rust编译器标志的字符串列表。
```

#### **Separate linking and dependencies for shared libraries**

```
  Shared libraries are special in that not all changes to them require that
  dependent targets be re-linked. If the shared library is changed but no
  imports or exports are different, dependent code needn't be relinked, which
  can speed up the build.

  If your link step can output a list of exports from a shared library and
  writes the file only if the new one is different, the timestamp of this file
  can be used for triggering re-links, while the actual shared library would be
  used for linking.

  You will need to specify
    restat = true
  in the linker tool to make this work, so Ninja will detect if the timestamp
  of the dependency file has changed after linking (otherwise it will always
  assume that running a command updates the output):
  共享库的特殊之处在于，并非所有对它们的更改都需要重新链接从属目标。如果共享库已更改，但没有导入或导出不同，则无需重新链接相关代码，这可以加快构建速度。       如果链接步骤可以从共享库中输出导出列表，并且仅在新文件不同时写入该文件，则该文件的时间戳可以用于触发重新链接，而实际的共享库将用于链接。              
  您需要指定              
  restat=真              
  在链接器工具中，Ninja将检测链接后依赖文件的时间戳是否发生了变化（否则，它将始终假设运行命令会更新输出）：

    tool("solink") {
      command = "..."
      outputs = [
        "{{output_dir}}/{{target_output_name}}{{output_extension}}",
        "{{output_dir}}/{{target_output_name}}"
            "{{output_extension}}.TOC",
      ]
      link_output =
        "{{output_dir}}/{{target_output_name}}{{output_extension}}"
      depend_output =
        "{{output_dir}}/{{target_output_name}}"
            "{{output_extension}}.TOC"
      restat = true
    }
```

#### **Example**

```
  toolchain("my_toolchain") {
    # Put these at the top to apply to all tools below.
    lib_switch = "-l"
    lib_dir_switch = "-L"

    tool("cc") {
      command = "gcc {{source}} -o {{output}}"
      outputs = [ "{{source_out_dir}}/{{source_name_part}}.o" ]
      description = "GCC {{source}}"
    }
    tool("cxx") {
      command = "g++ {{source}} -o {{output}}"
      outputs = [ "{{source_out_dir}}/{{source_name_part}}.o" ]
      description = "G++ {{source}}"
    }
  };
```
