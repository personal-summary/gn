### gn help toolchain
#### **toolchain**: Defines a toolchain.

```
  A toolchain is a set of commands and build flags used to compile the source
  code. The toolchain() function defines these commands.
  工具链是一组用于编译源代码的命令和构建标志。toolchain（）函数定义了这些命令。
```

#### **Toolchain overview**

```
  You can have more than one toolchain in use at once in a build and a target
  can exist simultaneously in multiple toolchains. A build file is executed
  once for each toolchain it is referenced in so the GN code can vary all
  parameters of each target (or which targets exist) on a per-toolchain basis.

  When you have a simple build with only one toolchain, the build config file
  is loaded only once at the beginning of the build. It must call
  set_default_toolchain() (see "gn help set_default_toolchain") to tell GN the
  label of the toolchain definition to use. The "toolchain_args" section of the
  toolchain definition is ignored.

  When a target has a dependency on a target using different toolchain (see "gn
  help labels" for how to specify this), GN will start a build using that
  secondary toolchain to resolve the target. GN will load the build config file
  with the build arguments overridden as specified in the toolchain_args.
  Because the default toolchain is already known, calls to
  set_default_toolchain() are ignored.

  To load a file in an alternate toolchain, GN does the following:

    1. Loads the file with the toolchain definition in it (as determined by the
       toolchain label).
    2. Re-runs the master build configuration file, applying the arguments
       specified by the toolchain_args section of the toolchain definition.
    3. Loads the destination build file in the context of the configuration file
       in the previous step.

  The toolchain configuration is two-way. In the default toolchain (i.e. the
  main build target) the configuration flows from the build config file to the
  toolchain. The build config file looks at the state of the build (OS type,
  CPU architecture, etc.) and decides which toolchain to use (via
  set_default_toolchain()). In secondary toolchains, the configuration flows
  from the toolchain to the build config file: the "toolchain_args" in the
  toolchain definition specifies the arguments to re-invoke the build.
  
  您可以在一个构建中同时使用多个工具链，并且一个目标可以同时存在于多个工具链条中。一个构建文件对于它所引用的每个工具链执行一次，因此GN代码可以在每个工具链的基础上改变每个目标（或存在哪些目标）的所有参数。              
  当您有一个只有一个工具链的简单构建时，构建配置文件只在构建开始时加载一次。它必须调用set_default_toolchain（）（请参阅“gn help set_default_toolchain”）来告诉gn要使用的工具链定义的标签。将忽略工具链定义的“toolchain_args”部分。              
  当目标依赖于使用不同工具链的目标时（请参阅“gn帮助标签”了解如何指定），gn将使用该辅助工具链开始构建以解析目标。GN将加载构建配置文件，其中包含在toolchain_args中指定的重写的构建参数。因为默认的工具链是已知的，所以将忽略对set_default_toolchain（）的调用。              
  要在备用工具链中加载文件，GN执行以下操作：              
  1.加载包含工具链定义的文件（由工具链标签确定）。              
  2.重新运行主构建配置文件，应用工具链定义的toolchain_args部分指定的参数。              
  3.在上一步骤中的配置文件上下文中加载目标生成文件。              
  工具链配置是双向的。在默认工具链（即主构建目标）中，配置从构建配置文件流到工具链。构建配置文件查看构建的状态（OS类型、CPU架构等），并决定使用哪个工具链（通过set_default_toolchain（））。在辅助工具链中，配置从工具链流到构建配置文件：工具链定义中的“toolchain_args”指定重新调用构建的参数。
```

#### **Functions and variables**

```
  tool()
    The tool() function call specifies the commands to run for a given step. See
    "gn help tool".

  toolchain_args [scope]
    Overrides for build arguments to pass to the toolchain when invoking it.
    This is a variable of type "scope" where the variable names correspond to
    variables in declare_args() blocks.

    When you specify a target using an alternate toolchain, the master build
    configuration file is re-interpreted in the context of that toolchain.
    toolchain_args allows you to control the arguments passed into this
    alternate invocation of the build.

    Any default system arguments or arguments passed in via "gn args" will also
    be passed to the alternate invocation unless explicitly overridden by
    toolchain_args.

    The toolchain_args will be ignored when the toolchain being defined is the
    default. In this case, it's expected you want the default argument values.

    See also "gn help buildargs" for an overview of these arguments.

  propagates_configs [boolean, default=false]
    Determines whether public_configs and all_dependent_configs in this
    toolchain propagate to targets in other toolchains.

    When false (the default), this toolchain will not propagate any configs to
    targets in other toolchains that depend on it targets inside this
    toolchain. This matches the most common usage of toolchains where they
    represent different architectures or compilers and the settings that apply
    to one won't necessarily apply to others.

    When true, configs (public and all-dependent) will cross the boundary out
    of this toolchain as if the toolchain boundary wasn't there. This only
    affects one direction of dependencies: a toolchain can't control whether
    it accepts such configs, only whether it pushes them. The build is
    responsible for ensuring that any external targets depending on targets in
    this toolchain are compatible with the compiler flags, etc. that may be
    propagated.

  deps [string list]
    Dependencies of this toolchain. These dependencies will be resolved before
    any target in the toolchain is compiled. To avoid circular dependencies
    these must be targets defined in another toolchain.

    This is expressed as a list of targets, and generally these targets will
    always specify a toolchain:
      deps = [ "//foo/bar:baz(//build/toolchain:bootstrap)" ]

    This concept is somewhat inefficient to express in Ninja (it requires a lot
    of duplicate of rules) so should only be used when absolutely necessary.
    工具（）              
    tool（）函数调用指定为给定步骤运行的命令。请参阅“gn帮助工具”。              
    工具链参数[scope]              
    调用工具链时传递给工具链的生成参数的重写。这是一个类型为“scope”的变量，其中变量名对应于declare_args（）块中的变量。              
    当您使用备用工具链指定目标时，主构建配置文件将在该工具链的上下文中重新解释。toolchainargs允许您控制传递到构建的这个替代调用中的参数。              任何默认系统参数或通过“gn args”传入的参数也将传递给备用调用，除非被toolchain_args显式覆盖。              
    当定义的工具链是默认值时，将忽略toolchain_args。在这种情况下，您需要默认的参数值。              
    有关这些参数的概述，请参见“gn-help-buildargs”。              
    propagates_configs[布尔值，默认值=false]              
    确定此工具链中的public_configs和all_dependent_configs是否传播到其他工具链中目标。              
    如果为false（默认值），此工具链将不会将任何配置传播到依赖于此工具链中目标的其他工具链中的目标。这与工具链的最常见用法相匹配，其中它们表示不同的体系结构或编译器，应用于其中一个的设置不一定适用于其他。              
    如果为true，配置（公共的和所有依赖的）将越过此工具链的边界，就像工具链边界不存在一样。这只影响依赖关系的一个方向：工具链不能控制它是否接受这样的配置，只能控制它是否推送它们。构建负责确保依赖于此工具链中目标的任何外部目标与可能传播的编译器标志等兼容。              
    deps[字符串列表]              
    此工具链的依赖项。在编译工具链中的任何目标之前，将解析这些依赖关系。为了避免循环依赖关系，这些必须是在另一个工具链中定义的目标。              
    这表示为目标列表，通常这些目标将始终指定一个工具链：              
    deps=[“//foo/bar:baz（//build/toolchain:bootstrap）”]              
    这个概念在忍者中表达起来有点低效（它需要大量重复规则），因此只能在绝对必要时使用。
```

#### **Example of defining a toolchain**

```
  toolchain("32") {
    tool("cc") {
      command = "gcc {{source}}"
      ...
    }

    toolchain_args = {
      use_doom_melon = true  # Doom melon always required for 32-bit builds.
      current_cpu = "x86"
    }
  }

  toolchain("64") {
    tool("cc") {
      command = "gcc {{source}}"
      ...
    }

    toolchain_args = {
      # use_doom_melon is not overridden here, it will take the default.
      current_cpu = "x64"
    }
  }
```

#### **Example of cross-toolchain dependencies**

```
  If a 64-bit target wants to depend on a 32-bit binary, it would specify a
  dependency using data_deps (data deps are like deps that are only needed at
  runtime and aren't linked, since you can't link a 32-bit and a 64-bit
  library).
  如果64位目标希望依赖于32位二进制文件，它将使用data_deps指定依赖项（数据dep类似于只在运行时需要的dep，并且不链接，因为无法链接32位和64位库）。

    executable("my_program") {
      ...
      if (target_cpu == "x64") {
        # The 64-bit build needs this 32-bit helper.
        data_deps = [ ":helper(//toolchains:32)" ]
      }
    }

    if (target_cpu == "x86") {
      # Our helper library is only compiled in 32-bits.
      shared_library("helper") {
        ...
      }
    }
```
