### gn help xcode_test_application_name
#### **xcode_test_application_name**: Name for Xcode test target.

```
  Each unit and ui test target must have a test application target, and this
  value is used to specify the relationship. Only meaningful to Xcode (used as
  part of the Xcode project generation).

  See "gn help create_bundle" for more information.
```

#### **Example**

```
  create_bundle("chrome_xctest") {
    test_application_name = "chrome"
    ...
  }
```
