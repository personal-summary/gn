### gn help aliased_deps
#### **aliased_deps**: [scope] Set of crate-dependency pairs.

```
  Valid for `rust_library` targets and `executable`, `static_library`, and
  `shared_library` targets that contain Rust sources.

  A scope, each key indicating the renamed crate and the corresponding value
  specifying the label of the dependency producing the relevant binary.

  All dependencies listed in this field *must* be listed as deps of the target.

    executable("foo") {
      sources = [ "main.rs" ]
      deps = [ "//bar" ]
    }

  This target would compile the `foo` crate with the following `extern` flag:
  `rustc ...command... --extern bar=<build_out_dir>/obj/bar`

    executable("foo") {
      sources = [ "main.rs" ]
      deps = [ ":bar" ]
      aliased_deps = {
        bar_renamed = ":bar"
      }
    }

  With the addition of `aliased_deps`, above target would instead compile with:
  `rustc ...command... --extern bar_renamed=<build_out_dir>/obj/bar`
```
