### gn help args
#### **The string "args" is both a command and a variable for action targets.**
#### **Showing help for both...**


#### **gn args**: (command-line tool)

```
  Display or configure arguments declared by the build.

    gn args <out_dir> [--list] [--short] [--args] [--overrides-only]

  See also "gn help buildargs" for a more high-level overview of how
  build arguments work.
```

#### **Usage**

```
  gn args <out_dir>
      Open the arguments for the given build directory in an editor. If the
      given build directory doesn't exist, it will be created and an empty args
      file will be opened in the editor. You would type something like this
      into that file:
          enable_doom_melon=false
          os="android"

      To find your editor on Posix, GN will search the environment variables in
      order: GN_EDITOR, VISUAL, and EDITOR. On Windows GN will open the command
      associated with .txt files.

      Note: you can edit the build args manually by editing the file "args.gn"
      in the build directory and then running "gn gen <out_dir>".

  gn args <out_dir> --list[=<exact_arg>] [--short] [--overrides-only] [--json]
      Lists all build arguments available in the current configuration, or, if
      an exact_arg is specified for the list flag, just that one build
      argument.

      The output will list the declaration location, current value for the
      build, default value (if different than the current value), and comment
      preceding the declaration.

      If --short is specified, only the names and current values will be
      printed.

      If --overrides-only is specified, only the names and current values of
      arguments that have been overridden (i.e. non-default arguments) will
      be printed. Overrides come from the <out_dir>/args.gn file and //.gn

      If --json is specified, the output will be emitted in json format.
      JSON schema for output:
      [
        {
          "name": variable_name,
          "current": {
            "value": overridden_value,
            "file": file_name,
            "line": line_no
          },
          "default": {
            "value": default_value,
            "file": file_name,
            "line": line_no
          },
          "comment": comment_string
        },
        ...
      ]
```

#### **Examples**

```
  gn args out/Debug
    Opens an editor with the args for out/Debug.

  gn args out/Debug --list --short
    Prints all arguments with their default values for the out/Debug
    build.

  gn args out/Debug --list --short --overrides-only
    Prints overridden arguments for the out/Debug build.

  gn args out/Debug --list=target_cpu
    Prints information about the "target_cpu" argument for the "
   "out/Debug
    build.

  gn args --list --args="os=\"android\" enable_doom_melon=true"
    Prints all arguments with the default values for a build with the
    given arguments set (which may affect the values of other
    arguments).
```

#### **\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\--\---**


#### **args**: (target variable) Arguments passed to an action.

```
  For action and action_foreach targets, args is the list of arguments to pass
  to the script. Typically you would use source expansion (see "gn help
  source_expansion") to insert the source file names.

  See also "gn help action" and "gn help action_foreach".
```
