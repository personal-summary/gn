### gn help bundle_root_dir
#### **bundle_root_dir**: Expansion of {{bundle_root_dir}} in create_bundle.

```
  A string corresponding to a path in root_build_dir.

  This string is used by the "create_bundle" target to expand the
  {{bundle_root_dir}} of the "bundle_data" target it depends on. This must
  correspond to a path under root_build_dir.
```

#### **Example**

```
  bundle_data("info_plist") {
    sources = [ "Info.plist" ]
    outputs = [ "{{bundle_contents_dir}}/Info.plist" ]
  }

  create_bundle("doom_melon.app") {
    deps = [ ":info_plist" ]
    bundle_root_dir = "${root_build_dir}/doom_melon.app"
    bundle_contents_dir = "${bundle_root_dir}/Contents"
    bundle_resources_dir = "${bundle_contents_dir}/Resources"
    bundle_executable_dir = "${bundle_contents_dir}/MacOS"
  }
```
