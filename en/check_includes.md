### gn help check_includes
#### **check_includes**: [boolean] Controls whether a target's files are checked.

```
  When true (the default), the "gn check" command (as well as "gn gen" with the
  --check flag) will check this target's sources and headers for proper
  dependencies.

  When false, the files in this target will be skipped by default. This does
  not affect other targets that depend on the current target, it just skips
  checking the includes of the current target's files.

  If there are a few conditionally included headers that trip up checking, you
  can exclude headers individually by annotating them with "nogncheck" (see "gn
  help nogncheck").

  The topic "gn help check" has general information on how checking works and
  advice on how to pass a check in problematic cases.
```

#### **Example**

```
  source_set("busted_includes") {
    # This target's includes are messed up, exclude it from checking.
    check_includes = false
    ...
  }
```
