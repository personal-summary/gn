### gn help clean
#### **gn clean &lt;out_dir&gt;**

```
  Deletes the contents of the output directory except for args.gn and
  creates a Ninja build environment sufficient to regenerate the build.
```
