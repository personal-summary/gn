### gn help code_signing_outputs
#### **code_signing_outputs**: [file list] Output files for code signing step.

```
  Outputs from the code signing step of a create_bundle target. Must refer to
  files in the build directory.

  See also "gn help create_bundle".
```
