### gn help code_signing_script
#### **code_signing_script**: [file name] Script for code signing."

```
  An absolute or buildfile-relative file name of a Python script to run for a
  create_bundle target to perform code signing step.

  See also "gn help create_bundle".
```
