### gn help code_signing_sources
#### **code_signing_sources**: [file list] Sources for code signing step.

```
  A list of files used as input for code signing script step of a create_bundle
  target. Non-absolute paths will be resolved relative to the current build
  file.

  See also "gn help create_bundle".
```
