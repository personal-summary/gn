### gn help contents
#### **contents**: Contents to write to file.

```
  The contents of the file for a generated_file target.
  See "gn help generated_file".
```
