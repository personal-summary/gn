### gn help crate_root
#### **crate_root**: [string] The root source file for a binary or library.

```
  Valid for `rust_library` targets and `executable`, `static_library`,
  `shared_library`, and `source_set` targets that contain Rust sources.

  This file is usually the `main.rs` or `lib.rs` for binaries and libraries,
  respectively.

  If crate_root is not set, then this rule will look for a lib.rs file (or
  main.rs for executable) or a single file in sources, if sources contains
  only one file.
```
