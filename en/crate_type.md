### gn help crate_type
#### **crate_type**: [string] The type of linkage to use on a shared_library.

```
  Valid for `rust_library` targets and `executable`, `static_library`,
  `shared_library`, and `source_set` targets that contain Rust sources.

  Options for this field are "cdylib", "staticlib", "proc-macro", and "dylib".
  This field sets the `crate-type` attribute for the `rustc` tool on static
  libraries, as well as the appropiate output extension in the
  `rust_output_extension` attribute. Since outputs must be explicit, the `lib`
  crate type (where the Rust compiler produces what it thinks is the
  appropriate library type) is not supported.

  It should be noted that the "dylib" crate type in Rust is unstable in the set
  of symbols it exposes, and most usages today are potentially wrong and will
  be broken in the future.

  Static libraries, rust libraries, and executables have this field set
  automatically.
```
