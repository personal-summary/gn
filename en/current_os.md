### gn help current_os
#### **current_os**: The operating system of the current toolchain.

```
  The build configuration usually sets this value based on the value of
  "target_os" (see "gn help target_os"), and then threads this through the
  toolchain definitions to ensure that it always reflects the appropriate
  value.

  This value is not used internally by GN for any purpose. It is set to the
  empty string ("") by default but is declared so that it can be overridden on
  the command line if so desired.

  See "gn help target_os" for a list of common values returned.
```
