### gn help current_toolchain
#### **current_toolchain**: Label of the current toolchain.

```
  A fully-qualified label representing the current toolchain. You can use this
  to make toolchain-related decisions in the build. See also
  "default_toolchain".
```

#### **Example**

```
  if (current_toolchain == "//build:64_bit_toolchain") {
    executable("output_thats_64_bit_only") {
      ...
```
