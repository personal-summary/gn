### gn help externs
#### **externs**: [scope] Set of Rust crate-dependency pairs.

```
  A list, each value being a scope indicating a pair of crate name and the path
  to the Rust library.

  These libraries will be passed as `--extern crate_name=path` to compiler
  invocation containing the current target.
```

#### **Examples**

```
    executable("foo") {
      sources = [ "main.rs" ]
      externs = [{
        crate_name = "bar",
        path = "path/to/bar.rlib"
      }]
    }

  This target would compile the `foo` crate with the following `extern` flag:
  `--extern bar=path/to/bar.rlib`.
```
