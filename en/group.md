### gn help group
#### **group**: Declare a named group of targets.

```
  This target type allows you to create meta-targets that just collect a set of
  dependencies into one named target. Groups can additionally specify configs
  that apply to their dependents.
```

#### **Variables**

```
  Deps: data_deps, deps, public_deps
  Dependent configs: all_dependent_configs, public_configs
```

#### **Example**

```
  group("all") {
    deps = [
      "//project:runner",
      "//project:unit_tests",
    ]
  }
```
