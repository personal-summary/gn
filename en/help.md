### gn help help
#### **gn help &lt;anything&gt;**

```
  Yo dawg, I heard you like help on your help so I put help on the help in the
  help.

  You can also use "all" as the parameter to get all help at once.
```

#### **Switches**

```
  --markdown
      Format output in markdown syntax.
```

#### **Example**

```
  gn help --markdown all
      Dump all help to stdout in markdown format.
```
