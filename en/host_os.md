### gn help host_os
#### **host_os**: [string] The operating system that GN is running on.

```
  This value is exposed so that cross-compiles can access the host build
  system's settings.

  This value should generally be treated as read-only. It, however, is not used
  internally by GN for any purpose.
```

#### **Some possible values**

```
  - "linux"
  - "mac"
  - "win"
```
