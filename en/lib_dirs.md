### gn help lib_dirs
#### **lib_dirs**: Additional library directories.

```
  A list of directories.

  Specifies additional directories passed to the linker for searching for the
  required libraries. If an item is not an absolute path, it will be treated as
  being relative to the current build file.

  libs and lib_dirs work differently than other flags in two respects.
  First, they are inherited across static library boundaries until a
  shared library or executable target is reached. Second, they are
  uniquified so each one is only passed once (the first instance of it
  will be the one used).
```

#### **Ordering of flags and values**

```
  1. Those set on the current target (not in a config).
  2. Those set on the "configs" on the target in order that the
     configs appear in the list.
  3. Those set on the "all_dependent_configs" on the target in order
     that the configs appear in the list.
  4. Those set on the "public_configs" on the target in order that
     those configs appear in the list.
  5. all_dependent_configs pulled from dependencies, in the order of
     the "deps" list. This is done recursively. If a config appears
     more than once, only the first occurence will be used.
  6. public_configs pulled from dependencies, in the order of the
     "deps" list. If a dependency is public, they will be applied
     recursively.

  For "libs" and "lib_dirs" only, the values propagated from
  dependencies (as described above) are applied last assuming they
  are not already in the list.
```

#### **Example**

```
  lib_dirs = [ "/usr/lib/foo", "lib/doom_melon" ]
```
