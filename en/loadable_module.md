### gn help loadable_module
#### **loadable_module**: Declare a loadable module target.

```
  This target type allows you to create an object file that is (and can only
  be) loaded and unloaded at runtime.

  A loadable module will be specified on the linker line for targets listing
  the loadable module in its "deps". If you don't want this (if you don't need
  to dynamically load the library at runtime), then you should use a
  "shared_library" target type instead.
```

#### **Language and compilation**

```
  The tools and commands used to create this target type will be
  determined by the source files in its sources. Targets containing
  multiple compiler-incompatible languages are not allowed (e.g. a
  target containing both C and C++ sources is acceptable, but a
  target containing C and Rust sources is not).
```

#### **Variables**

```
  Flags: cflags, cflags_c, cflags_cc, cflags_objc, cflags_objcc,
         asmflags, defines, include_dirs, inputs, ldflags, lib_dirs,
         libs, precompiled_header, precompiled_source, rustflags,
         rustenv
  Deps: data_deps, deps, public_deps
  Dependent configs: all_dependent_configs, public_configs
  General: check_includes, configs, data, friend, inputs, metadata,
           output_name, output_extension, public, sources, testonly,
           visibility
  Rust variables: aliased_deps, crate_root, crate_name, crate_type
```
