### gn help meta
#### **gn meta**

```
  gn meta <out_dir> <target>* --data=<key>[,<key>*]* [--walk=<key>[,<key>*]*]
          [--rebase=<dest dir>]

  Lists collected metaresults of all given targets for the given data key(s),
  collecting metadata dependencies as specified by the given walk key(s).

  See `gn help generated_file` for more information on the walk.
```

#### **Arguments**

```
  <target(s)>
    A list of target labels from which to initiate the walk.

  --data
    A list of keys from which to extract data. In each target walked, its metadata
    scope is checked for the presence of these keys. If present, the contents of
    those variable in the scope are appended to the results list.

  --walk (optional)
    A list of keys from which to control the walk. In each target walked, its
    metadata scope is checked for the presence of any of these keys. If present,
    the contents of those variables is checked to ensure that it is a label of
    a valid dependency of the target and then added to the set of targets to walk.
    If the empty string ("") is present in any of these keys, all deps and data_deps
    are added to the walk set.

  --rebase (optional)
    A destination directory onto which to rebase any paths found. If set, all
    collected metadata will be rebased onto this path. This option will throw errors
    if collected metadata is not a list of strings.
```

#### **Examples**

```
  gn meta out/Debug "//base/foo" --data=files
      Lists collected metaresults for the `files` key in the //base/foo:foo
      target and all of its dependency tree.

  gn meta out/Debug "//base/foo" --data=files --data=other
      Lists collected metaresults for the `files` and `other` keys in the
      //base/foo:foo target and all of its dependency tree.

  gn meta out/Debug "//base/foo" --data=files --walk=stop
      Lists collected metaresults for the `files` key in the //base/foo:foo
      target and all of the dependencies listed in the `stop` key (and so on).

  gn meta out/Debug "//base/foo" --data=files --rebase="/"
      Lists collected metaresults for the `files` key in the //base/foo:foo
      target and all of its dependency tree, rebasing the strings in the `files`
      key onto the source directory of the target's declaration relative to "/".
```
