### gn help output_conversion
#### **output_conversion**: Data format for generated_file targets.

```
  Controls how the "contents" of a generated_file target is formatted.
  See "gn help io_conversion".
```
