### gn help output_prefix_override
#### **output_prefix_override**: Don't use prefix for output name.

```
  A boolean that overrides the output prefix for a target. Defaults to false.

  Some systems use prefixes for the names of the final target output file. The
  normal example is "libfoo.so" on Linux for a target named "foo".

  The output prefix for a given target type is specified on the linker tool
  (see "gn help tool"). Sometimes this prefix is undesired.

  See also "gn help output_extension".
```

#### **Example**

```
  shared_library("doom_melon") {
    # Normally this will produce "libdoom_melon.so" on Linux. Setting this flag
    # will produce "doom_melon.so".
    output_prefix_override = true
    ...
  }
```
