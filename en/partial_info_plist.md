### gn help partial_info_plist
#### **partial_info_plist**: [filename] Path plist from asset catalog compiler.

```
  Valid for create_bundle target, corresponds to the path for the partial
  Info.plist created by the asset catalog compiler that needs to be merged
  with the application Info.plist (usually done by the code signing script).

  The file will be generated regardless of whether the asset compiler has
  been invoked or not. See "gn help create_bundle".
```
