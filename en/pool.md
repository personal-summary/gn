### gn help pool
#### **pool**: Defines a pool object.

```
  Pool objects can be applied to a tool to limit the parallelism of the
  build. This object has a single property "depth" corresponding to
  the number of tasks that may run simultaneously.

  As the file containing the pool definition may be executed in the
  context of more than one toolchain it is recommended to specify an
  explicit toolchain when defining and referencing a pool.

  A pool named "console" defined in the root build file represents Ninja's
  console pool. Targets using this pool will have access to the console's
  stdin and stdout, and output will not be buffered. This special pool must
  have a depth of 1. Pools not defined in the root must not be named "console".
  The console pool can only be defined for the default toolchain.
  Refer to the Ninja documentation on the console pool for more info.

  A pool is referenced by its label just like a target.
```

#### **Variables**

```
  depth*
  * = required
```

#### **Example**

```
  if (current_toolchain == default_toolchain) {
    pool("link_pool") {
      depth = 1
    }
  }

  toolchain("toolchain") {
    tool("link") {
      command = "..."
      pool = ":link_pool($default_toolchain)"
    }
  }
```
#### **pool**: Label of the pool used by the action.

```
  A fully-qualified label representing the pool that will be used for the
  action. Pools are defined using the pool() {...} declaration.
```

#### **Example**

```
  action("action") {
    pool = "//build:custom_pool"
    ...
  }
```
