### gn help print
#### **print**: Prints to the console.

```
  Prints all arguments to the console separated by spaces. A newline is
  automatically appended to the end.

  This function is intended for debugging. Note that build files are run in
  parallel so you may get interleaved prints. A buildfile may also be executed
  more than once in parallel in the context of different toolchains so the
  prints from one file may be duplicated or
  interleaved with itself.
```

#### **Examples**

```
  print("Hello world")

  print(sources, deps)
```
