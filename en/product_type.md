### gn help product_type
#### **product_type**: Product type for Xcode projects.

```
  Correspond to the type of the product of a create_bundle target. Only
  meaningful to Xcode (used as part of the Xcode project generation).

  When generating Xcode project files, only create_bundle target with a
  non-empty product_type will have a corresponding target in Xcode project.
```
