### gn help public_configs
#### **public_configs**: Configs to be applied on dependents.

```
  A list of config labels.

  Targets directly depending on this one will have the configs listed in this
  variable added to them. These configs will also apply to the current target.
  Generally, public configs are used to apply defines and include directories
  necessary to compile this target's header files.

  See also "gn help all_dependent_configs".
```

#### **Propagation of public configs**

```
  Public configs are applied to all targets that depend directly on this one.
  These dependant targets can further push this target's public configs
  higher in the dependency tree by depending on it via public_deps (see "gn
  help public_deps").

    static_library("toplevel") {
      # This target will get "my_config" applied to it. However, since this
      # target uses "deps" and not "public_deps", targets that depend on this
      # one won't get it.
      deps = [ ":intermediate" ]
    }

    static_library("intermediate") {
      # Depending on "lower" in any way will apply "my_config" to this target.
      # Additionall, since this target depends on "lower" via public_deps,
      # targets that depend on this one will also get "my_config".
      public_deps = [ ":lower" ]
    }

    static_library("lower") {
      # This will get applied to all targets that depend on this one.
      public_configs = [ ":my_config" ]
    }

  Public config propagation happens in a second phase once a target and all of
  its dependencies have been resolved. Therefore, a target will not see these
  force-added configs in their "configs" variable while the script is running,
  and they can not be removed. As a result, this capability should generally
  only be used to add defines and include directories rather than setting
  complicated flags that some targets may not want.

  Public configs may or may not be propagated across toolchain boundaries
  depending on the value of the propagates_configs flag (see "gn help
  toolchain") on the toolchain of the target declaring the public_config.
```

#### **Avoiding applying public configs to this target**

```
  If you want the config to apply to targets that depend on this one, but NOT
  this one, define an extra layer of indirection using a group:

    # External targets depend on this group.
    group("my_target") {
      # Config to apply to all targets that depend on this one.
      public_configs = [ ":external_settings" ]
      deps = [ ":internal_target" ]
    }

    # Internal target to actually compile the sources.
    static_library("internal_target") {
      # Force all external targets to depend on the group instead of directly
      # on this so the "external_settings" config will get applied.
      visibility = [ ":my_target" ]
      ...
    }
```

#### **Ordering of flags and values**

```
  1. Those set on the current target (not in a config).
  2. Those set on the "configs" on the target in order that the
     configs appear in the list.
  3. Those set on the "all_dependent_configs" on the target in order
     that the configs appear in the list.
  4. Those set on the "public_configs" on the target in order that
     those configs appear in the list.
  5. all_dependent_configs pulled from dependencies, in the order of
     the "deps" list. This is done recursively. If a config appears
     more than once, only the first occurence will be used.
  6. public_configs pulled from dependencies, in the order of the
     "deps" list. If a dependency is public, they will be applied
     recursively.
```
