### gn help public_deps
#### **public_deps**: Declare public dependencies.

```
  Public dependencies are like private dependencies (see "gn help deps") but
  additionally express that the current target exposes the listed deps as part
  of its public API.

  This has several ramifications:

    - public_configs that are part of the dependency are forwarded to direct
      dependents.

    - Public headers in the dependency are usable by dependents (includes do
      not require a direct dependency or visibility).

    - If the current target is a shared library, other shared libraries that it
      publicly depends on (directly or indirectly) are propagated up the
      dependency tree to dependents for linking.

  See also "gn help public_configs".
```

#### **Discussion**

```
  Say you have three targets: A -> B -> C. C's visibility may allow B to depend
  on it but not A. Normally, this would prevent A from including any headers
  from C, and C's public_configs would apply only to B.

  If B lists C in its public_deps instead of regular deps, A will now inherit
  C's public_configs and the ability to include C's public headers.

  Generally if you are writing a target B and you include C's headers as part
  of B's public headers, or targets depending on B should consider B and C to
  be part of a unit, you should use public_deps instead of deps.
```

#### **Example**

```
  # This target can include files from "c" but not from
  # "super_secret_implementation_details".
  executable("a") {
    deps = [ ":b" ]
  }

  shared_library("b") {
    deps = [ ":super_secret_implementation_details" ]
    public_deps = [ ":c" ]
  }
```
