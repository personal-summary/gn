### gn help python_path
#### **python_path**: Absolute path of Python.

```
  Normally used in toolchain definitions if running some command requires
  Python. You will normally not need this when invoking scripts since GN
  automatically finds it for you.
```
