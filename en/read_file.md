### gn help read_file
#### **read_file**: Read a file into a variable.

```
  read_file(filename, input_conversion)

  Whitespace will be trimmed from the end of the file. Throws an error if the
  file can not be opened.
```

#### **Arguments**

```
  filename
      Filename to read, relative to the build file.

  input_conversion
      Controls how the file is read and parsed. See "gn help io_conversion".
```

#### **Example**

```
  lines = read_file("foo.txt", "list lines")
```
