### gn help rebase
#### **rebase**: Rebase collected metadata as files.

```
  A boolean that triggers a rebase of collected metadata strings based on their
  declared file. Defaults to false.

  Metadata generally declares files as strings relative to the local build file.
  However, this data is often used in other contexts, and so setting this flag
  will force the metadata collection to be rebased according to the local build
  file's location and thus allow the filename to be used anywhere.

  Setting this flag will raise an error if any target's specified metadata is
  not a string value.

  See also "gn help generated_file".
```
