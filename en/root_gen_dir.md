### gn help root_gen_dir
#### **root_gen_dir**: Directory for the toolchain's generated files.

```
  Absolute path to the root of the generated output directory tree for the
  current toolchain. An example would be "//out/Debug/gen" for the default
  toolchain, or "//out/Debug/arm/gen" for the "arm" toolchain.

  This is primarily useful for setting up include paths for generated files. If
  you are passing this to a script, you will want to pass it through
  rebase_path() (see "gn help rebase_path") to convert it to be relative to the
  build directory.

  See also "target_gen_dir" which is usually a better location for generated
  files. It will be inside the root generated dir.
```
