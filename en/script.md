### gn help script
#### **script**: Script file for actions.

```
  An absolute or buildfile-relative file name of a Python script to run for a
  action and action_foreach targets (see "gn help action" and "gn help
  action_foreach").
```
