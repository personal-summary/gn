### gn help sources
#### **sources**: Source files for a target

```
  A list of files. Non-absolute paths will be resolved relative to the current
  build file.
```

#### **Sources for binary targets**

```
  For binary targets (source sets, executables, and libraries), the known file
  types will be compiled with the associated tools. Unknown file types and
  headers will be skipped. However, you should still list all C/C+ header files
  so GN knows about the existence of those files for the purposes of include
  checking.

  As a special case, a file ending in ".def" will be treated as a Windows
  module definition file. It will be appended to the link line with a
  preceding "/DEF:" string. There must be at most one .def file in a target
  and they do not cross dependency boundaries (so specifying a .def file in a
  static library or source set will have no effect on the executable or shared
  library they're linked into).

  For Rust targets that do not specify a crate_root, then the crate_root will
  look for a lib.rs file (or main.rs for executable) or a single file in
  sources, if sources contains only one file.
```

#### **Sources for non-binary targets**

```
  action_foreach
    The sources are the set of files that the script will be executed over. The
    script will run once per file.

  action
    The sources will be treated the same as inputs. See "gn help inputs" for
    more information and usage advice.

  copy
    The source are the source files to copy.
```
