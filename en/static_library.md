### gn help static_library
#### **static_library**: Declare a static library target.

```
  Make a ".a" / ".lib" file.

  If you only need the static library for intermediate results in the build,
  you should consider a source_set instead since it will skip the (potentially
  slow) step of creating the intermediate library file.
```

#### **Variables**

```
  complete_static_lib
  Flags: cflags, cflags_c, cflags_cc, cflags_objc, cflags_objcc,
         asmflags, defines, include_dirs, inputs, ldflags, lib_dirs,
         libs, precompiled_header, precompiled_source, rustflags,
         rustenv
  Deps: data_deps, deps, public_deps
  Dependent configs: all_dependent_configs, public_configs
  General: check_includes, configs, data, friend, inputs, metadata,
           output_name, output_extension, public, sources, testonly,
           visibility
  Rust variables: aliased_deps, crate_root, crate_name

  The tools and commands used to create this target type will be
  determined by the source files in its sources. Targets containing
  multiple compiler-incompatible languages are not allowed (e.g. a
  target containing both C and C++ sources is acceptable, but a
  target containing C and Rust sources is not).
```
