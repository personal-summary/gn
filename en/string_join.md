### gn help string_join
#### **string_join**: Concatenates a list of strings with a separator.

```
  result = string_join(separator, strings)

  Concatenate a list of strings with intervening occurrences of separator.
```

#### **Examples**

```
    string_join("", ["a", "b", "c"])    --> "abc"
    string_join("|", ["a", "b", "c"])   --> "a|b|c"
    string_join(", ", ["a", "b", "c"])  --> "a, b, c"
    string_join("s", ["", ""])          --> "s"
```
