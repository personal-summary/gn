### gn help target_cpu
#### **target_cpu**: The desired cpu architecture for the build.

```
  This value should be used to indicate the desired architecture for the
  primary objects of the build. It will match the cpu architecture of the
  default toolchain, but not necessarily the current toolchain.

  In many cases, this is the same as "host_cpu", but in the case of
  cross-compiles, this can be set to something different. This value is
  different from "current_cpu" in that it does not change based on the current
  toolchain. When writing rules, "current_cpu" should be used rather than
  "target_cpu" most of the time.

  This value is not used internally by GN for any purpose, so it may be set to
  whatever value is needed for the build. GN defaults this value to the empty
  string ("") and the configuration files should set it to an appropriate value
  (e.g., setting it to the value of "host_cpu") if it is not overridden on the
  command line or in the args.gn file.
```

#### **Possible values**

```
  - "x86"
  - "x64"
  - "arm"
  - "arm64"
  - "mipsel"
```
