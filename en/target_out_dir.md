### gn help target_out_dir
#### **target_out_dir**: [string] Directory for target output files.

```
  Absolute path to the target's generated file directory. If your current
  target is in "//tools/doom_melon" then this value might be
  "//out/Debug/obj/tools/doom_melon". It will not have a trailing slash.

  This is primarily useful for setting up arguments for calling scripts. If you
  are passing this to a script, you will want to pass it through rebase_path()
  (see "gn help rebase_path") to convert it to be relative to the build
  directory.

  See also "gn help root_out_dir".
```

#### **Example**

```
  action("myscript") {
    # Pass the output dir to the script.
    args = [ "-o", rebase_path(target_out_dir, root_build_dir) ]"
  }
```
