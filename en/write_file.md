### gn help write_file
#### **write_file**: Write a file to disk.

```
  write_file(filename, data, output_conversion = "")

  If data is a list, the list will be written one-item-per-line with no quoting
  or brackets.

  If the file exists and the contents are identical to that being written, the
  file will not be updated. This will prevent unnecessary rebuilds of targets
  that depend on this file.

  One use for write_file is to write a list of inputs to an script that might
  be too long for the command line. However, it is preferable to use response
  files for this purpose. See "gn help response_file_contents".
```

#### **Arguments**

```
  filename
      Filename to write. This must be within the output directory.

  data
      The list or string to write.

  output_conversion
    Controls how the output is written. See "gn help io_conversion".
```
