### gn help xcode_extra_attributes
#### **xcode_extra_attributes**: [scope] Extra attributes for Xcode projects.

```
  The value defined in this scope will be copied to the EXTRA_ATTRIBUTES
  property of the generated Xcode project. They are only meaningful when
  generating with --ide=xcode.

  See "gn help create_bundle" for more information.
```
